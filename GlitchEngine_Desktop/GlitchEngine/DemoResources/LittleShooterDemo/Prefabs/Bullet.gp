Bullet = {
	mt = {},
	gameObject = nil,

	Instantiate = function(position, direction)
		local prefab = {}
		prefab.gameObject = Sprite(position, 0, Vec2(24, 24), 3, false, "Bullet")
		AddComponent(prefab.gameObject, "BulletController")
		GetComponent(prefab.gameObject, "BulletController").SetDirection(direction)
		setmetatable(prefab, Bullet.mt)
		return prefab
	end
}

Bullet.mt.__metatable = "Private"

Bullet.mt.__index = function(table, key)
	print("Key Not Found")
end

Bullet.mt.__newindex = function(table, key, value)
	print("Prefabs Are Private")
end 

-- Destructor
Bullet.mt.__gc = function(self)
	Destroy(self.gameObject)
end