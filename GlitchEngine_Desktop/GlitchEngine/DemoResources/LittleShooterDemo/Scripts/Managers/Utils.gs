function Lerp(a, b, t)
	return a + (b - a) * t
end

function Vec2Lerp(a, b, t)
	local x = a.x + (b.x - a.x) * t
	local y = a.y + (b.y - a.y) * t 
	return Vec2(x,y)
end

function Vec2OutQuadEasing(a, b, t)
	local x = -(b.x-a.x) * t * (t - 2) + a.x
	local y = -(b.y-a.y) * t * (t - 2) + a.y
	return Vec2(x,y)
end

function RandomUnitCircle()
	local randomAngle = math.random() * math.pi * 2
	local randomX = math.cos(randomAngle)
	local randomY = math.sin(randomAngle)
	return Vec2(randomX, randomY)
end

defaultCategory = 1 << 0
playerCategory = 1 << 1
enemyCategory = 1 << 2
bulletCategory = 1 << 3

playerMask = defaultCategory
enemyMask = defaultCategory | bulletCategory
bulletMask = enemyCategory | defaultCategory

-- Config Booleans
camera = true
animation = true
shooting = true
fireRate = 0.1
variance = true
shake = true
knockback = true
enemiesSpawn = true
spawnInterval = 1
strafe = true
permanence = true
enemyKnockback = true