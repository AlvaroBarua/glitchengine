#version 330
in vec2 UV; 
uniform sampler2D texture;
uniform sampler2D other;
uniform float cutOff;

vec2 rOffset = vec2(0.0015,-0.0015);
vec2 gOffset = vec2(-0.0015,0.0015);
vec2 bOffset = vec2(0.0015,-0.0015);

out vec4 fragColor; 

void main () 
{

    vec4 color = texture2D(texture, UV);
    float alphaBlend = texture2D(other, UV).r;
    if(alphaBlend <= cutOff)
    {
		fragColor.r = dot(color.rgb, vec3(.393, .769, .189));
		fragColor.g = dot(color.rgb, vec3(.349, .686, .168));
		fragColor.b = dot(color.rgb, vec3(.272, .534, .131)); 
    	fragColor.a = color.a;
    }
    else
    	fragColor = vec4(0,0,0,1);
}