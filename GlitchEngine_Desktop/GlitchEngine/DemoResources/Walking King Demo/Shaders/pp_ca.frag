#version 330
in vec2 UV; 
uniform sampler2D texture;
uniform sampler2D other;
uniform float cutOff;

vec2 rOffset = vec2(0.0025,-0.0025);
vec2 gOffset = vec2(-0.0025,0.0025);
vec2 bOffset = vec2(0.0025,-0.0025);

out vec4 fragColor; 

void main () {

	vec4 rValue = texture2D(texture, UV - rOffset);  
	vec4 gValue = texture2D(texture, UV - gOffset);
	vec4 bValue = texture2D(texture, UV - bOffset);
	float alphaBlend = texture2D(other, UV).r;
		
    if(alphaBlend <= cutOff)
    	fragColor = vec4(rValue.r, gValue.g, bValue.b, 1.0);
    else
    	fragColor = vec4(0,0,0,1);
}