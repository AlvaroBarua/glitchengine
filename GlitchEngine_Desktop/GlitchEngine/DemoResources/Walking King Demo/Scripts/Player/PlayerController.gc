local playerFSM = nil
local animationIndex = nil
local transform = nil
local material = nil
local rigidbody = nil
local isLookingLeft = false
local isLookingRight = true
local lives = 3
local isInControl = false
local playerSpeed = 5
local enTextTrigger = false
local lastLinearVelocity = nil
local startTime = 0
local translateRight = false
local translateLeft = false
local startX = 0
local dieSFX = nil
local levelIndex = 0
local jumpSFX = nil
local jumpHit = nil
local winSFX = nil

function Start()

	winSFX = SoundSource("Win")
	dieSFX = SoundSource("Die")
	jumpSFX = SoundSource("Jump")
	jumpHit = SoundSource("JumpHit")

	transform = gameObject:GetTransform()
	material = gameObject:GetMaterial()
	
	animationIndex = SpriteAnimator:RegisterQuad(gameObject, "King")
	SpriteAnimator:SetAnimationScale(animationIndex, Vec2(4,4))
	SpriteAnimator:SetAnimationSpeed(animationIndex, 6)
	SpriteAnimator:RegisterAnimation(animationIndex, "Idle", "Walk_", 1, 1, 2, "")
	SpriteAnimator:RegisterAnimation(animationIndex, "Walk", "Walk_", 1, 4, 2, "")
	SpriteAnimator:RegisterAnimation(animationIndex, "Run", "Run_", 1, 4, 2, "")
	SpriteAnimator:RegisterAnimation(animationIndex, "Climb", "Climb_", 1, 4, 2, "")
	SpriteAnimator:RegisterAnimation(animationIndex, "Hit", "Hit_", 1, 3, 2, "")
	SpriteAnimator:RegisterAnimation(animationIndex, "Punch", "Punch_", 1, 3, 2, "")
	SpriteAnimator:RegisterAnimation(animationIndex, "Jump_Down", "Jump_Down", 0, 0, 0, "")
	SpriteAnimator:RegisterAnimation(animationIndex, "Jump_Up", "Jump_Up", 0, 0, 0, "")
	SpriteAnimator:RegisterAnimation(animationIndex, "Landing", "Landing", 0, 0, 0, "")
	SpriteAnimator:PlayAnimation(animationIndex, "Idle", false)

	SpriteAnimator:RegisterFrameEvent(animationIndex, "Hit", 3, HitAnimEnded)
	SpriteAnimator:RegisterFrameEvent(animationIndex, "Punch", 3, PunchAnimEnded)


	playerFSM = FSM()
	playerFSM:RegisterCallback(FSMCallback)
	playerFSM:AddState("Idle")
	playerFSM:AddState("Walk")
	playerFSM:AddState("Run")
	playerFSM:AddState("Jump_Down")
	playerFSM:AddState("Jump_Up")
	playerFSM:AddState("Landing")
	playerFSM:AddState("Hit")
	playerFSM:AddState("Punch")
	playerFSM:SetStartState("Idle")

	playerFSM:SetParameter("ZERO", 0)
	playerFSM:SetParameter("ONE", 1)
	playerFSM:SetParameter("hSpeed", 0)
	playerFSM:SetParameter("vSpeed", 0)
	playerFSM:SetParameter("walkSpeed", playerSpeed)
	playerFSM:SetParameter("beenHit", 0)
	playerFSM:SetParameter("punch", 0)

	playerFSM:AddTransition("Idle","Walk",FSMO_G,"hSpeed","ZERO")
	playerFSM:AddTransition("Walk","Idle",FSMO_EQ,"hSpeed","ZERO")

	playerFSM:AddTransition("Walk","Run",FSMO_G,"hSpeed","walkSpeed")
	playerFSM:AddTransition("Run","Walk",FSMO_LEQ,"hSpeed","walkSpeed")

	playerFSM:AddTransition("Idle","Jump_Up",FSMO_G,"vSpeed","ZERO")
	playerFSM:AddTransition("Idle","Jump_Down",FSMO_L,"vSpeed","ZERO")
	playerFSM:AddTransition("Walk","Jump_Up",FSMO_G,"vSpeed","ZERO")
	playerFSM:AddTransition("Walk","Jump_Down",FSMO_L,"vSpeed","ZERO")
	playerFSM:AddTransition("Run","Jump_Up",FSMO_G,"vSpeed","ZERO")
	playerFSM:AddTransition("Run","Jump_Down",FSMO_L,"vSpeed","ZERO")

	playerFSM:AddTransition("Jump_Up","Jump_Down",FSMO_L,"vSpeed","ZERO")
	playerFSM:AddTransition("Jump_Down","Landing",FSMO_EQ,"vSpeed","ZERO")

	playerFSM:AddTransition("Landing","Idle",FSMO_EQ,"hSpeed","ZERO")
	playerFSM:AddTransition("Landing","Walk",FSMO_G,"hSpeed","ZERO")
	playerFSM:AddTransition("Landing","Run",FSMO_G,"hSpeed","walkSpeed")

	playerFSM:AddTransition("Idle","Hit",FSMO_EQ,"beenHit","ONE")
	playerFSM:AddTransition("Hit","Idle",FSMO_EQ,"beenHit","ZERO")

	playerFSM:AddTransition("Walk","Hit",FSMO_EQ,"beenHit","ONE")
	playerFSM:AddTransition("Run","Hit",FSMO_EQ,"beenHit","ONE")
	playerFSM:AddTransition("Jump_Up","Hit",FSMO_EQ,"beenHit","ONE")
	playerFSM:AddTransition("Jump_Down","Hit",FSMO_EQ,"beenHit","ONE")

	playerFSM:AddTransition("Idle","Punch",FSMO_EQ,"punch","ONE")
	playerFSM:AddTransition("Punch","Idle",FSMO_EQ,"punch","ZERO")

	playerFSM:AddTransition("Walk","Punch",FSMO_EQ,"punch","ONE")
	playerFSM:AddTransition("Run","Punch",FSMO_EQ,"punch","ONE")
end

function Update()
	SoundListener.SetPosition(transform.position)
	jumpHit:SetPosition(transform.position)
	jumpSFX:SetPosition(transform.position)
	
	if isInControl == true then
		local onGround = (playerFSM.currentState == "Idle" or playerFSM.currentState == "Walk" or playerFSM.currentState == "Run")
		local runMod = Input:GetKey(KEY_LSHIFT) and onGround

		rigidbody.linearVelocity = Vec2(0, rigidbody.linearVelocity.y)

		if Input:GetKey(KEY_RIGHT) then
			LookRight()
			if runMod then
				rigidbody.linearVelocity = Vec2(playerSpeed * 2, rigidbody.linearVelocity.y)
			else
				rigidbody.linearVelocity = Vec2(playerSpeed, rigidbody.linearVelocity.y)
			end
		end

		if Input:GetKey(KEY_LEFT) then
			LookLeft()
			if runMod then
				rigidbody.linearVelocity = Vec2(-playerSpeed * 2, rigidbody.linearVelocity.y)
			else
				rigidbody.linearVelocity = Vec2(-playerSpeed, rigidbody.linearVelocity.y)
			end
		end

		if Input:GetKeyDown(KEY_SPACE) and onGround then
			jumpSFX:Play(false)
			rigidbody:ApplyLinearImpulse(Vec2(0, 160))
		end

		if Input:GetKeyDown(KEY_F1) then
			playerFSM:SetParameter("beenHit", 1)
		end

		if Input:GetKeyDown(KEY_F2) then
			playerFSM:SetParameter("punch", 1)
		end

		playerFSM:SetParameter("hSpeed", math.abs(rigidbody.linearVelocity.x))
		playerFSM:SetParameter("vSpeed", rigidbody.linearVelocity.y)

		if enTextTrigger then
			if Input:GetKeyDown(KEY_E) then
				GUIController.ShowTextPanel()
			end
		end
	end

	if translateRight then
		local deltaAnim = (Time.time - startTime) / 1.5
		local newX = Lerp(startX, startX + 64, deltaAnim)
		transform.position = Vec2(newX, transform.position.y)
		if deltaAnim >= 1 then
			translateRight = false
			transform.position = Vec2(startX + 64, transform.position.y)
			rigidbody.active = true
			rigidbody.linearVelocity = lastLinearVelocity
			isInControl = true
		end
	end

	if translateLeft then
		local deltaAnim = (Time.time - startTime) / 1.5
		local newX = Lerp(startX, startX - 64, deltaAnim)
		transform.position = Vec2(newX, transform.position.y)
		if deltaAnim >= 1 then
			translateLeft = false
			transform.position = Vec2(startX - 64, transform.position.y)
			rigidbody.active = true
			rigidbody.linearVelocity = lastLinearVelocity
			isInControl = true
		end
	end
end

function FSMCallback()
	SpriteAnimator:PlayAnimation(animationIndex, playerFSM.currentState, true)
end

function GetAnimationIndex()
	return animationIndex
end

function LookLeft()
	if isLookingLeft == false then
		material:SetVec2("flip",Vec2(1,0))
		isLookingLeft = true
		isLookingRight = false
	end
end

function LookRight()
	if isLookingRight == false then
		material:SetVec2("flip",Vec2(0,0))
		isLookingLeft = false
		isLookingRight = true
	end
end

function GetLives()
	return lives
end

function GainControl()

	rigidbody = gameObject:AddRigidBody(DYNAMIC_BODY)
	rigidbody.fixedRotation = true
	rigidbody:AddBoxCollider(Vec2(12 * 4, 22 * 4), Vec2(0, 0))
	rigidbody:SetContactEnterCallback(OnContactEnter)
	rigidbody:SetContactExitCallback(OnContactExit)
	rigidbody:SetPreSolveCallback(OnContactPreSolve)

	isInControl = true
end

function HitAnimEnded()
	playerFSM:SetParameter("beenHit", 0)
end

function PunchAnimEnded()
	playerFSM:SetParameter("punch", 0)
end

function OnContactEnter(info)
	if info.gameObject.tag == "Floor" and info.normal.y > 0.5 and info.impactVelocity > 2 then
		jumpHit:Play(false)
	end

	if info.gameObject.tag == "InfoBox" then
		GUIController.ShowEButton()
		enTextTrigger = true
	end

	if info.gameObject.tag == "RightExit" then
		levelIndex = levelIndex + 1
		GUIController.HideTut()
		isInControl = false
		lastLinearVelocity = rigidbody.linearVelocity
		rigidbody.active = false
		startX = transform.position.x
		translateRight = true
		startTime = Time.time
		CameraController.CameraTranslateRight()
		GameManager.LoadNextLevel()
	end

	if info.gameObject.tag == "LeftExit" and not translateRight then
		levelIndex = levelIndex - 1
		isInControl = false
		lastLinearVelocity = rigidbody.linearVelocity
		rigidbody.active = false
		startX = transform.position.x
		startTime = Time.time
		translateLeft = true
		CameraController.CameraTranslateLeft()
	end

	if info.gameObject.tag == "DieBlock" then
		dieSFX:Play(false)
		lives = lives - 1
		isInControl = false
		if lives <= 0 then
			GameManager.EndGame()
		else
			GameManager.ResetLevel()
		end
	end

	if info.gameObject.tag == "Finish" then
		isInControl = false
		winSFX:SetPosition(transform.position)
		winSFX:Play(false)
		GameManager.EndGame()
	end
end

function OnContactExit(info)
	if info.gameObject.tag == "InfoBox" then
		enTextTrigger = false
		GUIController.HideEButton()
		GUIController.HideTextPanel()
	end
end

function OnContactPreSolve(info, contactData)
	if info.gameObject.tag == "Floor" then

		if info.normal.y < 0.5 then
			contactData:SetEnabled(false)
		end

		if info.normal.y > 0.5 and info.impactVelocity < 0 and math.abs(info.impactVelocity) > 2 then
			contactData:SetEnabled(false)
		end
	end
end

function ResetPlayer()
	LookRight()
	isInControl = true
	rigidbody.active = true
	transform.position = Vec2(-480 + 64 + 960 * levelIndex, -82)
	rigidbody.linearVelocity = Vec2(0,0)
end