local level = {}
local sfx_Rock = nil
local tileSetCols
local texelSize
local lastLoadedMap = nil
local lastMapName = nil

LevelLoader = {
	RenderLayer = function(map, layerName, sortOrder, topRight)
		local mapTileSize = (map.height * map.width) - 1
		tileSetCols = map:GetTileset("Env_Tiles").columns
		texelSize = Vec2(1 / map:GetTileset("Env_Tiles").imageWidth , 1 / map:GetTileset("Env_Tiles").imageHeight)
		local layer = map:GetLayer(layerName)

		level[lastMapName][layerName] = {}
		level[lastMapName][layerName].mats = {}
		level[lastMapName][layerName].quads = {}

		local x = 0
		local y = 0
		for i = 0, mapTileSize do
			local tile = layer:GetTile(i) - 1
			if tile ~= -1 then
				local indexY = math.floor(tile / tileSetCols)
				local indexX = tile - (indexY * tileSetCols)

				if level[lastMapName][layerName].mats[tile] == nil then
					level[lastMapName][layerName].mats[tile] = Material("GE_Sprited", false)
					level[lastMapName][layerName].mats[tile]:SetTexture("texture","Env_Tiles")
					level[lastMapName][layerName].mats[tile]:SetVec2("texel", texelSize)
					level[lastMapName][layerName].mats[tile]:SetVec4("frame",Vec4(indexX * map.tileWidth, indexY * map.tileHeight, map.tileWidth, map.tileHeight))
				end

				level[lastMapName][layerName].quads[i] = Quad(topRight + Vec2(64 * x, 64 * y),0,Vec2(64, 64),sortOrder,level[lastMapName][layerName].mats[tile])
			end
			x = x + 1
			if x >= map.width then
				x = 0
				y = y - 1
			end
		end
	end,

	RenderScene = function(mapName, layerNames, offset)
		lastMapName = mapName
		lastLoadedMap = TiledMapParser:ParseFile("Maps/"..mapName..".json")
		local startPoint = Vec2(-480 + 32, 320 - 32) + offset

		level[lastMapName] = {}
		for k,v in pairs(layerNames) do
			LevelLoader.RenderLayer(lastLoadedMap, v, k, startPoint)
		end

		GameManager.DoneLoadingScene(mapName)
	end,

	DeleteLastLoadedLevel = function()
		for k,v in pairs(level) do
			for k2,v2 in pairs(v) do
				for k3,v3 in pairs(v2) do
					for k4,v4 in pairs(v3) do
						Destroy(v4)
					end
				end
			end
		end
	end,

	PlayBlockingPathAnimSFX = function()
		sfx_Rock = SoundSource("sfx_Rock")
		sfx_Rock:SetPosition(Vec2(-460, 0))
		sfx_Rock:Play(false)
	end,

	SpawnTile = function(tile, position, sortOrder)
		local go = {}
		local indexY = math.floor(tile / tileSetCols)
		local indexX = tile - (indexY * tileSetCols)

		go.mat = Material("GE_Sprited", false)
		go.mat:SetTexture("texture","Env_Tiles")
		go.mat:SetVec2("texel", texelSize)
		go.mat:SetVec4("frame",Vec4(indexX * 16, indexY * 16, 16, 16))
		go.quad = Quad(position, 0, Vec2(64, 64), sortOrder, go.mat)
		return go
	end,

	SpawnLevelCollision = function(layer, offset)
		local startPoint = Vec2(-480, 320) + offset
		local layer = lastLoadedMap:GetLayer(layer)
			if layer.type == "objectgroup" then
				level[lastMapName][layer] = {}
				for i = 0, layer.objectCount - 1 do
					local object = layer:GetTiledObject(i)
					if object ~= nil then
						local collider = nil
						local realStartPoint = startPoint + Vec2(object.x * 4, -object.y * 4)
						level[lastMapName][layer][i] = {}
						level[lastMapName][layer][i].go = GameObject()
						level[lastMapName][layer][i].go.tag = object.name
						level[lastMapName][layer][i].rb = level[lastMapName][layer][i].go:AddRigidBody(STATIC_BODY)
						if object.polygon == true then
							local points = {}
							for k = 0, object.pointCount - 1 do
								local point = object:GetPoint(k)
								points[k] = realStartPoint + Vec2(point.x, -point.y):ScalarMult(4)
							end
							collider = level[lastMapName][layer][i].rb:AddPolygonCollider(points)
						end
						
						if object.type == "Trigger" then
							collider.sensor = true
						end
					end
				end
			end
	end
}