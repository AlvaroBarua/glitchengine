local level = {}
local tileSetCols
local texelSize
local lastLoadedMap = nil
local lastMapName = nil

LevelLoader = {

	RenderScene = function(mapName, layerNames, offset, doneFunction)
		lastMapName = mapName
		lastLoadedMap = TiledMapParser:ParseFile("Map/"..mapName..".json")

		level[lastMapName] = {}
		for k,v in pairs(layerNames) do
			LevelLoader.RenderLayer(lastLoadedMap, v, 0, offset + Vec2(4,4))
		end

		doneFunction()
	end,

	RenderLayer = function(map, layerName, sortOrder, topRight)
		local mapTileSize = (map.height * map.width) - 1
		tileSetCols = map:GetTileset("Tile").columns
		texelSize = Vec2(1 / map:GetTileset("Tile").imageWidth , 1 / map:GetTileset("Tile").imageHeight)
		local layer = map:GetLayer(layerName)

		level[lastMapName][layerName] = {}
		level[lastMapName][layerName].mats = {}
		level[lastMapName][layerName].quads = {}

		local x = 0
		local y = 0
		for i = 0, mapTileSize do
			local tile = layer:GetTile(i) - 1
			if tile ~= -1 then
				local indexY = math.floor(tile / tileSetCols)
				local indexX = tile - (indexY * tileSetCols)

				if level[lastMapName][layerName].mats[tile] == nil then
					level[lastMapName][layerName].mats[tile] = Material("GE_Sprited", false)
					level[lastMapName][layerName].mats[tile]:SetTexture("texture","Tile")
					level[lastMapName][layerName].mats[tile]:SetVec2("texel", texelSize)
					level[lastMapName][layerName].mats[tile]:SetVec4("frame",Vec4(indexX * map.tileWidth, indexY * map.tileHeight, map.tileWidth, map.tileHeight))
				end

				level[lastMapName][layerName].quads[i] = Quad(topRight + Vec2(8 * x, 8 * y), 0, Vec2(8, 8), sortOrder, level[lastMapName][layerName].mats[tile])
			end

			x = x + 1
			if x >= map.width then
				x = 0
				y = y - 1
			end
		end
	end,

	SpawnCollision = function(layerName, topRight)
		level[lastMapName][layerName] = {}
		level[lastMapName][layerName].colliders = {}
		
		local collisionLayer = lastLoadedMap:GetLayer(layerName)
		for i = 0, collisionLayer.objectCount - 1 do
			local object = collisionLayer:GetTiledObject(i)
			level[lastMapName][layerName].colliders[i] = GameObject()
			level[lastMapName][layerName].colliders[i].tag = object:GetProperty("Tag")
			level[lastMapName][layerName].colliders[i]:GetTransform().position = topRight + Vec2(object.x, -object.y)
			local rb = level[lastMapName][layerName].colliders[i]:AddRigidBody(STATIC_BODY)
			local collider
			if object.width > object.height then
				collider = rb:AddBoxCollider(Vec2(object.width, object.height), Vec2(object.width, object.height))
			else
				collider = rb:AddBoxCollider(Vec2(object.width, object.height), Vec2(object.width, -object.height))
			end
			collider:SetFilterCategory(defaultCategory)
		end
	end
}