#pragma once
#include <Classes/PhysicsWorld.h>
#include <Classes/Transform.h>
#include <Utils/LuaEventDispatcher.h>
#include <Utils/FixtureWrapper.h>
#include <Box2D/Box2D.h>
#include <vector>

class GameObject;
struct CollisionInfo
{
	int pointCount;
	std::vector<Vector<glm::vec2>> points;
	Vector<glm::vec2> normal;
	float impactVelocity;
	GameObject* gameObject;

	Vector<glm::vec2> GetPoint(int index)
	{
		return points[index];
	}
};

class RigidBody
{
private:

	int contactEnterCallback;
	int contactExitCallback;
	int preSolveCallback;

	bool contactEnterRegistered;
	bool contactExitRegistered;
	bool preSolveRegistered;

	PhysicsWorld* world;
	Transform* transform;
	b2Body* body;
	std::vector<FixtureWrapper*> colliders;
	GameObject* parentGameObject;

public:
	RigidBody();
	RigidBody(Transform* t, b2BodyType type, GameObject* parent);
	~RigidBody();

	void Update();

	void SetActive(bool state);
	bool GetActiveState() const;

	void SetBodyType(unsigned int type);
	unsigned int GetBodyType() const;

	void SetGavityScale(float scale);
	float GetGavityScale() const;

	void SetFixedRotation(bool flag);
	bool GetFixedRotation() const;

	void SetLinearVelocity(Vector<glm::vec2> velocity);
	Vector<glm::vec2>  GetLinearVelocity() const;

	void SetAngularVelocity(float velocity);
	float  GetAngularVelocity() const;

	void SetLinearDamping(float dampening);
	float GetLinearDamping() const;

	void SetAngularDamping(float dampening);
	float GetAngularDamping() const;

	void SetBullet(bool bullet);
	bool GetBullet() const;

	void SetTransform(Vector<glm::vec2> position, float rotation);

	FixtureWrapper* AddBoxCollider(Vector<glm::vec2> size, Vector<glm::vec2> offset);
	FixtureWrapper* AddCircleCollider(float radius, Vector<glm::vec2> offset);
	FixtureWrapper* AddPolygonCollider();

	void SetOnContactEnterCallback();
	void SetOnContactExitCallback();
	void SetPreSolveCallback();

	void CallContactEnter(CollisionInfo col);
	void CallContactExit(CollisionInfo col);
	void CallPreSolve(CollisionInfo col, b2Contact* contact);

	void ApplyLinearImpulse(Vector<glm::vec2> impulse);
	void ApplyAngularImpulse(float impulse);
	void ApplyForce(Vector<glm::vec2> force);
	void ApplyTorque(float torque);
	GameObject* GetParentGameObject();
};