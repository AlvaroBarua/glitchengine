#include <Classes/PhysicsWorld.h>


PhysicsWorld &PhysicsWorld::GetInstance()
{
	static PhysicsWorld* ins;
	if (ins == NULL)
		ins = new PhysicsWorld();
	return *ins;
}

PhysicsWorld::PhysicsWorld()
{
	debugDraw = false;
	positionIterations = 3;
	velocityIterations = 8;
	timeStep = 1.f / 60.f;

	gravity = b2Vec2(0.f, -9.81f);
	world = new b2World(gravity);
}

void PhysicsWorld::Destroy()
{
	delete &GetInstance();
}

PhysicsWorld::~PhysicsWorld()
{
}

void PhysicsWorld::SetGravity(Vector<glm::vec2> newGravity)
{
	gravity = b2Vec2(newGravity.Get<0>(), newGravity.Get<1>());
	world->SetGravity(gravity);
}

b2World* PhysicsWorld::GetWorld()
{
	return world;
}

void PhysicsWorld::Step()
{
	world->Step(timeStep, velocityIterations, positionIterations);
}

float PhysicsWorld::GetSimulationUnits()
{
	return simulationUnits;
}

bool PhysicsWorld::GetDebugDraw() const
{
	return debugDraw;
}

void PhysicsWorld::SetDebugDraw(bool d)
{
	debugDraw = d;
}

void PhysicsWorld::SetSimulationUnitConversion(float ratio)
{
	simulationUnits = ratio;
}