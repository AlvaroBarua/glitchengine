#include "RigidBody.h"
#include <Objects/GameObject.h>

RigidBody::RigidBody()
{

}

RigidBody::RigidBody(Transform *t, b2BodyType type, GameObject* parent)
{
	parentGameObject = parent;
	world = &PhysicsWorld::GetInstance();
	contactEnterCallback = -1;
	contactExitCallback = -1;
	contactEnterRegistered = false;
	contactExitRegistered = false;
	preSolveRegistered = false;

	transform = t;
	b2BodyDef def;
	def.position = b2Vec2(transform->WorldPosition().Get<0>() / world->GetSimulationUnits(), transform->WorldPosition().Get<1>() / world->GetSimulationUnits());
	def.angle = glm::radians(transform->WorldRotation());
	def.type = type;
	def.allowSleep = true;
	def.awake = true;

	body = world->GetWorld()->CreateBody(&def);
	body->SetUserData(this);
}

RigidBody::~RigidBody()
{
	for (auto const &it : colliders)
		body->DestroyFixture(it->GetFixture());
	world->GetWorld()->DestroyBody(body);
}

void RigidBody::Update()
{
	transform->SetPositionFromRigidBody(Vector<glm::vec2>(body->GetPosition().x, body->GetPosition().y).ScalarMultiply(world->GetSimulationUnits()));
	transform->SetRotationFromRigidBody(-glm::degrees(body->GetAngle()));
}

void RigidBody::SetActive(bool state)
{
	body->SetActive(state);
}

bool RigidBody::GetActiveState() const
{
	return body->IsActive();
}

void RigidBody::SetBodyType(unsigned int type)
{
	body->SetType((b2BodyType)type);
}

unsigned int RigidBody::GetBodyType() const
{
	return body->GetType();
}

void RigidBody::SetGavityScale(float scale)
{
	body->SetGravityScale(scale);
}

float RigidBody::GetGavityScale() const
{
	return body->GetGravityScale();
}

void RigidBody::SetFixedRotation(bool flag)
{
	body->SetFixedRotation(flag);
}

bool RigidBody::GetFixedRotation() const
{
	return body->IsFixedRotation();
}

void RigidBody::SetLinearVelocity(Vector<glm::vec2> velocity)
{
	b2Vec2 v(velocity.Get<0>(), velocity.Get<1>());
	body->SetLinearVelocity(v);
}

Vector<glm::vec2> RigidBody::GetLinearVelocity() const
{
	b2Vec2 v = body->GetLinearVelocity();
	return Vector<glm::vec2>(v.x, v.y);
}

void RigidBody::SetAngularVelocity(float velocity)
{
	body->SetAngularVelocity(velocity);
}

float RigidBody::GetAngularVelocity() const
{
	return body->GetAngularVelocity();
}

void RigidBody::SetLinearDamping(float dampening)
{
	body->SetLinearDamping(dampening);
}

float RigidBody::GetLinearDamping() const
{
	return body->GetLinearDamping();
}

void RigidBody::SetAngularDamping(float dampening)
{
	body->SetAngularDamping(dampening);
}

float RigidBody::GetAngularDamping() const
{
	return body->GetAngularDamping();
}

void RigidBody::SetBullet(bool bullet)
{
	body->SetBullet(bullet);
}

bool RigidBody::GetBullet() const
{
	return body->IsBullet();
}

void RigidBody::SetTransform(Vector<glm::vec2> position, float rotation)
{
	float simRatio = world->GetSimulationUnits();
	b2Vec2 vec(position.Get<0>() / simRatio, position.Get<1>() / simRatio);
	body->SetTransform(vec, -glm::radians(rotation));
}

FixtureWrapper* RigidBody::AddBoxCollider(Vector<glm::vec2> size, Vector<glm::vec2> offset)
{
	float hX = size.Get<0>() / 2.f;
	float hY = size.Get<1>() / 2.f;

	float hOffsetX = offset.Get<0>() / 2.f;
	float hOffsetY = offset.Get<1>() / 2.f;

	b2PolygonShape shape;
	shape.SetAsBox(hX / world->GetSimulationUnits(), hY / world->GetSimulationUnits(), b2Vec2(hOffsetX / world->GetSimulationUnits(), hOffsetY / world->GetSimulationUnits()), 0);
	colliders.push_back(new FixtureWrapper(body->CreateFixture(&shape, 1.f)));
	return colliders[colliders.size() - 1];
}

FixtureWrapper* RigidBody::AddCircleCollider(float radius, Vector<glm::vec2> offset)
{
	b2CircleShape shape;
	shape.m_p.Set(offset.Get<0>() / world->GetSimulationUnits(), offset.Get<1>() / world->GetSimulationUnits());
	shape.m_radius = radius / world->GetSimulationUnits();
	colliders.push_back(new FixtureWrapper(body->CreateFixture(&shape, 1.f)));
	return colliders[colliders.size() - 1];
}

FixtureWrapper * RigidBody::AddPolygonCollider()
{
	b2PolygonShape shape;
	vector<b2Vec2> points;

	lua_State* L = (&LuaEventDispatcher::GetInstance())->GetLuaState();
	lua_pushnil(L);
	while (lua_next(L, -2))
	{
		luabridge::LuaRef ref(L);
		ref = luabridge::LuaRef::fromStack(L, -1);
		Vector<glm::vec2> point = ref.cast<Vector<glm::vec2>>();
		points.push_back(b2Vec2(point.Get<0>() / world->GetSimulationUnits(), point.Get<1>() / world->GetSimulationUnits()));
		lua_pop(L, 1);
	}

	shape.Set(&points[0], points.size());
	colliders.push_back(new FixtureWrapper(body->CreateFixture(&shape, 1.f)));
	return colliders[colliders.size() - 1];
}

void RigidBody::SetOnContactEnterCallback()
{
	contactEnterCallback = (&LuaEventDispatcher::GetInstance())->GetLuaFunctionReference();
	contactEnterRegistered = true;
}

void RigidBody::SetOnContactExitCallback()
{
	contactExitCallback = (&LuaEventDispatcher::GetInstance())->GetLuaFunctionReference();
	contactExitRegistered = true;
}

void RigidBody::SetPreSolveCallback()
{
	preSolveCallback = (&LuaEventDispatcher::GetInstance())->GetLuaFunctionReference();
	preSolveRegistered = true;
}

void RigidBody::CallContactEnter(CollisionInfo col)
{
	if (contactEnterRegistered)
		(&LuaEventDispatcher::GetInstance())->QueueLuaCollisionEvent(contactEnterCallback, col);
}

void RigidBody::CallContactExit(CollisionInfo col)
{
	if (contactExitRegistered)
		(&LuaEventDispatcher::GetInstance())->QueueLuaCollisionEvent(contactExitCallback, col);
}

void RigidBody::CallPreSolve(CollisionInfo col, b2Contact* contact)
{
	if (preSolveRegistered)
		(&LuaEventDispatcher::GetInstance())->CallPreSolveEvent(preSolveCallback, col, contact);
}

void RigidBody::ApplyLinearImpulse(Vector<glm::vec2> impulse)
{
	b2Vec2 v(impulse.Get<0>(), impulse.Get<1>());
	body->ApplyLinearImpulse(v, body->GetWorldCenter(), true);
}

void RigidBody::ApplyAngularImpulse(float impulse)
{
	body->ApplyAngularImpulse(-impulse, true);
}

void RigidBody::ApplyForce(Vector<glm::vec2> force)
{
	b2Vec2 v(force.Get<0>(), force.Get<1>());
	body->ApplyForceToCenter(v, true);
}

void RigidBody::ApplyTorque(float torque)
{
	body->ApplyTorque(-torque, true);
}

GameObject* RigidBody::GetParentGameObject()
{
	return parentGameObject;
}