#pragma once

#include <map>
#include <Utils/Logger.h>
#include <Utils/Application.h>
#include <Utils/Vector.h>
#include <Utils/ShaderLoader.h>
#include <Utils/ResourceManager.h>

class Renderer;
class Material
{
private:
	string shader;
	int vertexArrayPosition;
	GLuint uniqueTextureCombID;
	ResourceManager *RM;

	map<string, int> intMap;
	map<string, float> floatMap;
	map<string, glm::vec2> vec2Map;
	map<string, glm::vec3> vec3Map;
	map<string, glm::vec4> vec4Map;
	map<string, glm::mat4> mat4Map;
	map<string, GLuint> textures;

	GLuint fontTextureID;
	vector<int> parentsPositions;
	bool haveToUpdate;
	bool isInitialized;
	bool translucent;

public:

	Material(string shader, bool _translucent);
	Material();
	~Material();

	void SetInt(string key, int value);
	void SetFloat(string key, float value);
	void SetVec2(string key, Vector<glm::vec2> value);
	void SetVec3(string key, Vector<glm::vec3> value);
	void SetVec4(string key, Vector<glm::vec4> value);
	void SetTexture(string key, string fileName);
	void SetMovieTextures(GLuint yBuffer, GLuint uBuffer, GLuint vBuffer);
	void SetFontTexture(string fontName, FT_Face face, unsigned widht, unsigned height);
	void AddParentPosition(int pos);
	void CheckForUpdate();
	void UpdateBuffers();
	void ClearMaps();

	GLuint GetShader();
	GLuint GetArrayPosition();
	GLuint GetFontTextureID();

	void InitializeShader(bool fromTextureFunction);
};