#pragma once
#define GLM_FORCE_RADIANS

#include <vector>

#include <Utils/Application.h>
#include <Utils/TimeHandle.h>
#include <Utils/Vector.h>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/rotate_vector.hpp>

enum Space {
	SelfSpace = 0,
	WorldSpace = 1,
};

class GameObject;
class Renderer;

class Transform
{
private:
	bool haveToUpdate;
	int cameraFix;
	glm::mat4 translationMatrix;
	glm::mat4 rotationMatrix;
	glm::mat4 scaleMatrix;

	glm::mat4 worldMatrix;

	Vector<glm::vec2> scaleCorrection;

	float localRotation;
	Vector<glm::vec2> localScale;

	float worldRotation;
	Vector<glm::vec2> worldScale;
	Vector<glm::vec2> worldPosition;

	GameObject* gameObject;
	void CheckForUpdate();

	Transform* parentTransform;
	std::vector<Transform*> children;

	void ResetCameraScale();

public:
	Transform();
	Transform(bool isCam, GameObject* p);
	~Transform();

	Vector<glm::vec2> WorldPosition() const;
	float WorldRotation() const;
	Vector<glm::vec2> WorldScale() const;

	Vector<glm::vec2> LocalPosition() const;
	float LocalRotation() const;
	Vector<glm::vec2> LocalScale() const;

	void Rotate(float angle);
	void SetPosition(Vector<glm::vec2> newPos);
	void SetPositionFromRigidBody(Vector<glm::vec2> newPos);
	void SetRotation(float rot);
	void SetRotationFromRigidBody(float rot);
	void SetScale(Vector<glm::vec2> scaleVector);
	void Translate(Vector<glm::vec2> trans, unsigned short space);
	void ScaleObject(Vector<glm::vec2> scaleVec);
	void SetLayer(short layer);
	short GetLayer() const;
	void ResetUpdateStatus();
	bool UpdateStatus();
	void UpdateScaleCorrection();
	glm::mat4 GetMatrix();

	void SetParent(Transform* p);
	int ChildCount();
	Transform* GetChildAt(int index);
	GameObject* GetGameObject();
	void RemoveChildAt(int index);
	void RemoveChild(Transform* child);
	void DecomposeWorldMAtrix();
	void RevertLocalMatrix();
	void CameraUpdatedSize();
};