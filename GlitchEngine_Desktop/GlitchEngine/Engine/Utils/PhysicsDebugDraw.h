#pragma once
#include <glew.h>
#include <Box2D/Box2D.h>
#include <Objects/Camera.h>
#include <Classes/PhysicsWorld.h>

class PhysicsDebugDraw : public b2Draw
{
private:
	float unitConversion;
	Camera* mainCamera;
public:

	PhysicsDebugDraw();
	~PhysicsDebugDraw();

	void InitDraw();
	void EndDraw();

	void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
	void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
	void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
	void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
	void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
	void DrawTransform(const b2Transform& xf);
};

