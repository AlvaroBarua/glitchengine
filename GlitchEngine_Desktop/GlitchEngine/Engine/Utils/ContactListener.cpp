#include "ContactListener.h"


ContactListener::ContactListener()
{
}


ContactListener::~ContactListener()
{
}

void ContactListener::BeginContact(b2Contact* contact)
{
	CollisionInfo col, col2 = GetCollisionInfo(contact);
	col = col2;

	RigidBody* bodyA = static_cast<RigidBody*>(contact->GetFixtureA()->GetBody()->GetUserData());
	RigidBody* bodyB = static_cast<RigidBody*>(contact->GetFixtureB()->GetBody()->GetUserData());

	col.gameObject = bodyA->GetParentGameObject();
	col2.gameObject = bodyB->GetParentGameObject();

	bodyA->CallContactEnter(col2);
	bodyB->CallContactEnter(col);
}

void ContactListener::EndContact(b2Contact* contact)
{
	CollisionInfo col, col2 = GetCollisionInfo(contact);
	col = col2;

	RigidBody* bodyA = static_cast<RigidBody*>(contact->GetFixtureA()->GetBody()->GetUserData());
	RigidBody* bodyB = static_cast<RigidBody*>(contact->GetFixtureB()->GetBody()->GetUserData());

	col.gameObject = bodyA->GetParentGameObject();
	col2.gameObject = bodyB->GetParentGameObject();

	bodyA->CallContactExit(col2);
	bodyB->CallContactExit(col);
}

void ContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
	CollisionInfo col, col2 = GetCollisionInfo(contact);
	col = col2;

	RigidBody* bodyA = static_cast<RigidBody*>(contact->GetFixtureA()->GetBody()->GetUserData());
	RigidBody* bodyB = static_cast<RigidBody*>(contact->GetFixtureB()->GetBody()->GetUserData());

	col.gameObject = bodyA->GetParentGameObject();
	col2.gameObject = bodyB->GetParentGameObject();

	bodyA->CallPreSolve(col2, contact);
	bodyB->CallPreSolve(col, contact);
}

CollisionInfo ContactListener::GetCollisionInfo(b2Contact* contact)
{
	b2WorldManifold wManifold;
	contact->GetWorldManifold(&wManifold);

	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();

	b2Vec2 vel1 = fixtureA->GetBody()->GetLinearVelocityFromWorldPoint(wManifold.points[0]);
	b2Vec2 vel2 = fixtureB->GetBody()->GetLinearVelocityFromWorldPoint(wManifold.points[0]);

	CollisionInfo col;
	col.pointCount = contact->GetManifold()->pointCount;
	col.impactVelocity = b2Dot(vel1 - vel2, wManifold.normal);
	col.normal = Vector<glm::vec2>(wManifold.normal.x, wManifold.normal.y);

	for (int i = 0; i < col.pointCount; i++)
	{
		Vector<glm::vec2> point(wManifold.points[i].x, wManifold.points[i].y);
		col.points.push_back(point);
	}

	return col;
}
