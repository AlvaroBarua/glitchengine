#include "FixtureWrapper.h"

FixtureWrapper::FixtureWrapper(b2Fixture* fixt)
{
	fixture = fixt;
}

b2Fixture * FixtureWrapper::GetFixture()
{
	return fixture;
}

FixtureWrapper::~FixtureWrapper()
{
	delete fixture;
}

bool FixtureWrapper::IsSensor() const
{
	return fixture->IsSensor();
}

void FixtureWrapper::SetSensor(bool sensor)
{
	fixture->SetSensor(sensor);
}

float FixtureWrapper::GetFriction() const
{
	return fixture->GetFriction();
}

void FixtureWrapper::SetFriction(float friction)
{
	fixture->SetFriction(friction);
}

float FixtureWrapper::GetRestitution() const
{
	return fixture->GetRestitution();
}

void FixtureWrapper::SetRestitution(float restitution)
{
	fixture->SetRestitution(restitution);
}

bool FixtureWrapper::TestPoint(Vector<glm::vec2> point)
{
	return fixture->TestPoint(b2Vec2(point.Get<0>(), point.Get<1>()));
}

b2Filter FixtureWrapper::GetFilterData() const
{
	return fixture->GetFilterData();
}

void FixtureWrapper::SetFilterData(const b2Filter& data)
{
	fixture->SetFilterData(data);
}

void FixtureWrapper::SetFilterGroup(uint16 group)
{
	b2Filter data = fixture->GetFilterData();
	data.groupIndex = group;
	fixture->SetFilterData(data);
}

void FixtureWrapper::SetFilterMask(uint16 mask)
{
	b2Filter data = fixture->GetFilterData();
	data.maskBits = mask;
	fixture->SetFilterData(data);
}

void FixtureWrapper::SetFilterCategory(uint16 category)
{
	b2Filter data = fixture->GetFilterData();
	data.categoryBits = category;
	fixture->SetFilterData(data);
}

void FixtureWrapper::FlipShape()
{
	b2Shape* shape = fixture->GetShape();
	if (shape->GetType() == b2Shape::e_polygon)
	{
		b2PolygonShape* polygon = static_cast<b2PolygonShape*>(shape);
		for (int i = 0; i < polygon->GetVertexCount(); i++) {
			polygon->m_vertices[i].x *= -1;
		}
			
		b2Vec2* reVert = new b2Vec2[polygon->GetVertexCount()];
		int j = polygon->GetVertexCount() - 1;
		for (int i = 0; i < polygon->GetVertexCount(); i++)
			reVert[i] = polygon->m_vertices[j--];
		polygon->Set(&reVert[0], polygon->GetVertexCount());
	}
}
