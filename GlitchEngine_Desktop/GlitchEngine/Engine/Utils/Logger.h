#pragma once
#include <stdio.h>
#include <stdarg.h>

class Logger
{
public:

	static void RedirectToFile()
	{
		freopen("Resources/log.txt", "wt", stdout);
		freopen("Resources/log.txt", "wt", stderr);
	}

	static void Log(const char* format, ...)
	{
		va_list argList;
		va_start(argList, format);

		static char s_buffer[BUFSIZ];
		vsnprintf(s_buffer, BUFSIZ, format, argList);
		va_end(argList);
		
		printf("%s\n", s_buffer);
		fflush(stdout);
		fflush(stderr);
	}

	static void SimplePrint(const char* text)
	{
		printf("%s\n", text);
		fflush(stdout);
		fflush(stderr);
	}

	static void Terminate()
	{
		fflush(stderr);
		fflush(stdout);
	}
};