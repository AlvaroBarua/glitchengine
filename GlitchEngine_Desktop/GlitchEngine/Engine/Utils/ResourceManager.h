#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define QUEUEBUFFERS 4

#define STBI_NO_PSD
#define STBI_NO_GIF
#define STBI_NO_HDR
#define STBI_NO_PIC
#define STBI_NO_PNM

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <cstdio>
#include <string>
#include <map>
#include <sstream>
#include <algorithm>
#include <thread>
#include <atomic>

#include <Utils/Logger.h>
#include <Utils/WindowHandler.h>
#include <Utils/LuaEventDispatcher.h>
#include <Utils/ShaderLoader.h>

#include <stb_image.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <AL/al.h>
#include <freetype.h>

#ifdef __linux__
#include <dirent.h>
#elif WIN32
#include <windows.h>
#endif

using namespace std;

enum ImageFormat
{
	Transparent = 0,
	Solid = 1,
};

enum FunctionType
{
	Sound = 0,
	Font = 1,
	AnimationJSon = 2,
	Texture = 3,
};

struct TextureInfo
{
	unsigned char* image;
	ImageFormat format;
	int components;
	int textW;
	int textH;
	GLuint bufferID;
};

struct SoundInfo
{
	unsigned int bufferID;
	ALuint queueBuffersIDs[QUEUEBUFFERS];
	bool isOgg;
	string fileLocation;
};

struct FunctionParameters
{
	string key;
	string path;
	unsigned int filterMode;
	unsigned int wrapMode;
};

typedef pair<FunctionType, FunctionParameters> FunctionPointer;

class ResourceManager
{

private:

	FT_Library ft;
	LuaEventDispatcher* LED;
	thread processThread;

	int functionReference;
	bool queueIsOn;

	#ifdef __linux__
    void IterateDirsUnix(const char* entryPoint);
	#elif WIN32
	void IterateDirsWin(LPCWSTR entryPoint);
	#endif

	unsigned int LoadWav(string filename);
	ShaderLoader *loader;
	vector<FunctionPointer> functionQueue;
	atomic_int doneLoading;
	void ThreadLoading();
	
public:
	ResourceManager();
	~ResourceManager();

	static ResourceManager &GetInstance();
	int BinaryCharToInt(char *buff, int len);

	void LoadSound(string key, string path);
	void LoadFont(string key, string path);
	void LoadAnimation(string key, string path);
	void LoadTexture(string key, string path, unsigned int filterMode, unsigned int wrapMode);
	
	void RegisterLoadingCallback();
	void EnebleLoadingQueue();
	void ProcessLoadingQueue();

	void Destroy();

	bool isLittleEndian;
	map<string, SoundInfo> sounds;
	map<string, TextureInfo> textures;
	map<string, FT_Face> faces;
	map<string, string> luaFileNames;
	map<string, string> glitchComponents;
	vector<string> glitchPrefabs;
	map<string, string> animJsonFileLocations;

	void CheckForThread();
};