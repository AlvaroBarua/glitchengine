#pragma once

#include <stdio.h>

class TimeHandle
{
    private:
        TimeHandle();
        ~TimeHandle();
		double time;
		double delta;

    public:
        double rawDeltaTime;
		
		void SetTime(double counter);
		double GetTime() const;
		double GetDelta() const;

        static TimeHandle &GetInstance();
        void Destroy();
};