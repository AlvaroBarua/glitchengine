#pragma once
#include <vector>
#include <Objects/MovieQuad.h>

using namespace std;

class MovieQuadManager
{
private:
	static vector<MovieQuad*> quadList;
public:
	static void UpdateMovieQuads(double deltaTime)
	{
		bool shouldCheck = false;
		for (vector<MovieQuad*>::iterator it = quadList.begin(); it != quadList.end();)
		{
			if ((*it)->HasStartedPlaying())
			{
				if (!(*it)->GetStatus())
					(*it)->MovieUpdateLoop(deltaTime);
				else
					shouldCheck = true;
			}
			++it;
		}

		if (shouldCheck)
			RemoveFromList();
	}

	static void AddToList(MovieQuad* quad)
	{
		vector<MovieQuad*>::iterator it = find(quadList.begin(), quadList.end(), quad);
		if (it == quadList.end())
			quadList.push_back(quad);
	}

	static void RemoveFromList()
	{
		auto predicate = [](MovieQuad* q)
		{
			if (q->GetStatus() && q->HasStartedPlaying())
				return true;
			return false;
		};

		quadList.erase(std::remove_if(quadList.begin(), quadList.end(), predicate), quadList.end());
	}
};

