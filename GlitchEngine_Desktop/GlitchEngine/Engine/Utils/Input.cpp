#include <Utils/Input.h>

Input &Input::GetInstance()
{
	static Input *input = NULL;

	if (input == NULL)
		input = new Input();

	return *input;
}

void Input::Destroy()
{

	Input *iw = &GetInstance();
	if (iw != NULL)
		delete iw;
}

Input::Input()
{
	state = SDL_GetKeyboardState(NULL);
	scrollWheel = 0;
}

Input::~Input()
{ }

bool Input::PollEvents()
{
	bool quit = false;
	bool scrollEventCalled = false;
	while (SDL_PollEvent(&e) != 0)
	{

		switch(e.type)
		{
			case SDL_JOYBUTTONDOWN:
			case SDL_JOYBUTTONUP:
				joyEventMap[make_pair(Joystick, e.jbutton.which)] = make_pair(e.type, e.jbutton.button);
				break;

			case SDL_JOYAXISMOTION:
				joyEventMap[make_pair(Joystick, e.jaxis.which)] = make_pair(e.jaxis.axis, e.jaxis.value);
				break;

			case SDL_MOUSEWHEEL:
				scrollEventCalled = true;
				scrollWheel = e.wheel.y;
				break;

			case SDL_QUIT:
				quit = true;
				break;
		}
	}

	if (!scrollEventCalled)
		scrollWheel = 0;

	return quit;
}


bool Input::GetKeyDown(int key)
{
	if (!keyDownRegistered[key])
		keyDownRegistered[key] = true;

	if (state[key] && !keyPressedMap[key])
	{
		keyPressedMap[key] = true;
		return true;
	}
	else if (!keyUpRegistered[key] && !state[key])
		keyPressedMap[key] = false;

	return false;
}

bool Input::GetKeyUp(int key)
{
	if (!keyUpRegistered[key])
		keyUpRegistered[key] = true;

	if (!state[key] && keyPressedMap[key])
	{
		keyPressedMap[key] = false;
		return true;
	}
	else if (!keyDownRegistered[key] && state[key])
		keyPressedMap[key] = true;

	return false;
}

bool Input::GetKey(int key)
{
	GetKeyDown(key);
	return keyPressedMap[key];
}

Vector<glm::vec2> Input::GetScreenCursorPosition()
{
	int posX, posY;
	SDL_GetMouseState(&posX, &posY);
	return Vector<glm::vec2>(posX, posY);
}

Vector<glm::vec2> Input::GetWorldCursorPosition()
{
	int posX, posY;
	SDL_GetMouseState(&posX, &posY);

	glm::vec3 screenPos = glm::vec3(posX, posY, 0);
	glm::vec4 viewPort = glm::vec4(0.f, 0.f, (&ApplicationData::GetInstance())->renderWidth, (&ApplicationData::GetInstance())->renderHeight);
	glm::vec3 worldPos = glm::unProject(screenPos, Camera::GetMainCamera()->GetViewMatrix(), Camera::GetMainCamera()->GetProjectionMatrix(), viewPort);
	return Vector<glm::vec2>(worldPos.x, -worldPos.y);
}

bool Input::GetMouseButtonDown(int button)
{
	if (!mouseDownRegistered[button])
		mouseDownRegistered[button] = true;

	bool pressed = (bool)((SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(button)));
	if (pressed && !mousePressedMap[button])
	{
		mousePressedMap[button] = true;
		return true;
	}
	else if (!pressed && !mouseUpRegistered[button])
		mousePressedMap[button] = false;
	
	return false;
}

bool Input::GetMouseButton(int button)
{
	GetMouseButtonDown(button);
	return mousePressedMap[button];
}

bool Input::GetMouseButtonUp(int button)
{
	if (!mouseUpRegistered[button])
		mouseUpRegistered[button] = true;

	bool pressed = (bool)((SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(button)));
	if (!pressed && mousePressedMap[button])
	{
		mousePressedMap[button] = false;
		return true;
	}
	else if (pressed && !mouseDownRegistered[button])
		mousePressedMap[button] = true;

	return false;
}

int Input::GetScrollWheel()
{
	return scrollWheel;
}

int Input::GetFirstAvailableController()
{
	int joysticks = SDL_NumJoysticks();
	SDL_Joystick *joy;

	for (int i = 0; i < joysticks; i++)
	{
		joy = SDL_JoystickOpen(i);
		if (joy)
			return i;
	}

	return -1;
}


bool Input::GetJoystickButtonDown(int joystick, int button)
{
	if (!joystickButtonDownRegistered[joystick][button])
		joystickButtonDownRegistered[joystick][button] = true;

	pair<int,int> jbutton = joyEventMap[make_pair(Joystick,joystick)];
	if (jbutton.first == SDL_JOYBUTTONDOWN && jbutton.second == button && !joystickPressedMap[joystick][button])
	{
		joystickPressedMap[joystick][button] = true;
		return true;
	}
	else if (!joystickButtonUpRegistered[joystick][button] && jbutton.first == SDL_JOYBUTTONUP)
		joystickPressedMap[joystick][button] = false;

	return false;
}


bool Input::GetJoystickButtonUp(int joystick, int button)
{
	if (!joystickButtonUpRegistered[joystick][button])
		joystickButtonUpRegistered[joystick][button] = true;

	pair<int,int> jbutton = joyEventMap[make_pair(Joystick, joystick)];
	if (jbutton.first == SDL_JOYBUTTONUP && jbutton.second == button && joystickPressedMap[joystick][button])
	{
		joystickPressedMap[joystick][button] = false;
		return true;
	}
	else if (!joystickButtonDownRegistered[joystick][button] && jbutton.first == SDL_JOYBUTTONDOWN)
		joystickPressedMap[joystick][button] = true;

	return false;
}


bool Input::GetJoystickButton(int joystick, int button)
{
	GetJoystickButtonDown(joystick, button);
	return joystickPressedMap[joystick][button];
}


float Input::GetJoystickAxis(int joystick, int axis)
{
	pair<int, int> jaxis = joyEventMap[make_pair(Joystick, joystick)];
	if (jaxis.first == axis)
	{
		float value = -jaxis.second / 32767.0f;
		joystickAxisValue[make_pair(joystick, axis)] = value;
		return value;
	}
	else
		return joystickAxisValue[make_pair(joystick, axis)];
	
	return 0.f;
}