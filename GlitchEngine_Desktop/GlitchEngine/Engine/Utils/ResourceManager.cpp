#define STB_IMAGE_IMPLEMENTATION

#include <Utils/ResourceManager.h>

ResourceManager::ResourceManager()
{
	loader = new ShaderLoader();
	LED = &LuaEventDispatcher::GetInstance();

	queueIsOn = false;

	int endianTest = 1;
	isLittleEndian = (((char*)&endianTest)[0]);

	if (FT_Init_FreeType(&ft))
		Logger::Log("Error Initializing FreeType");

#ifdef WIN32
	IterateDirsWin(L"Resources/*");
#elif __linux__
	IterateDirsUnix("Resources");
#endif
}

ResourceManager &ResourceManager::GetInstance()
{
	static ResourceManager *ins = NULL;
	if (ins == NULL)
		ins = new ResourceManager();

	return *ins;
}

ResourceManager::~ResourceManager()
{
	for (auto const& it : sounds)
	{
		if (&it.second.isOgg)
			alDeleteBuffers(QUEUEBUFFERS, it.second.queueBuffersIDs);
		else
			alDeleteBuffers(1, &it.second.bufferID);
	}
}

void ResourceManager::Destroy()
{
	ResourceManager *rm = &GetInstance();
	if (rm != NULL)
		delete rm;
}

#ifdef WIN32
void ResourceManager::IterateDirsWin(LPCWSTR entryPoint)
{
	HANDLE dp;
	WIN32_FIND_DATA dirp;

	dp = FindFirstFile(entryPoint, &dirp);

	if (dp != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (wcscmp(dirp.cFileName, L".") != 0 && wcscmp(dirp.cFileName, L"..") != 0)
			{
				if (dirp.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					wstring entryMinusMask = wstring(entryPoint);
					entryMinusMask.resize(entryMinusMask.length() - 2);
					wstring newEntryPoint = entryMinusMask + L"/" + wstring(dirp.cFileName) + L"/*";
					IterateDirsWin(newEntryPoint.c_str());
				}
				else
				{
					wstring ws(dirp.cFileName);
					string str(ws.begin(), ws.end());

					stringstream ss(str);
					string item;
					vector<string> parts;
					while (getline(ss, item, '.'))
						parts.push_back(item);

					std::transform(parts[1].begin(), parts[1].end(), parts[1].begin(), ::toupper);

					if (parts[1] == "GS")
					{
						wstring entryMinusMask = wstring(entryPoint);
						entryMinusMask.resize(entryMinusMask.length() - 2);
						wstring fullFileLocation = entryMinusMask + L"/" + wstring(dirp.cFileName);
						string fullFileLocationString(fullFileLocation.begin(), fullFileLocation.end());
						luaFileNames[fullFileLocationString] = string(parts[0]);
					}

					if (parts[1] == "GC")
					{
						wstring entryMinusMask = wstring(entryPoint);
						entryMinusMask.resize(entryMinusMask.length() - 2);
						wstring fullFileLocation = entryMinusMask + L"/" + wstring(dirp.cFileName);
						string fullFileLocationString(fullFileLocation.begin(), fullFileLocation.end());
						glitchComponents[fullFileLocationString] = string(parts[0]);
					}

					if (parts[1] == "GP")
					{
						wstring entryMinusMask = wstring(entryPoint);
						entryMinusMask.resize(entryMinusMask.length() - 2);
						wstring fullFileLocation = entryMinusMask + L"/" + wstring(dirp.cFileName);
						string fullFileLocationString(fullFileLocation.begin(), fullFileLocation.end());
						glitchPrefabs.push_back(fullFileLocationString);
					}

					if (parts[1] == "FRAG")
					{
						wstring entryMinusMask = wstring(entryPoint);
						entryMinusMask.resize(entryMinusMask.length() - 2);
						wstring fullFileLocation = entryMinusMask + L"/" + wstring(dirp.cFileName);
						string fullFileLocationString(fullFileLocation.begin(), fullFileLocation.end() - 5);

						loader->LoadShader(parts[0], fullFileLocationString);
					}
				}
			}
		} while (FindNextFile(dp, &dirp) != 0);
	}

	FindClose(dp);
}
#endif

#ifdef __linux__
void ResourceManager::IterateDirsUnix(const char* entryPoint)
{
	DIR *dp = opendir(entryPoint);
	struct dirent *dirp;

	if (dp == NULL)
		Logger::Log("Error abriendo carpeta \n");

	dirp = readdir(dp);
	while (dirp != NULL)
	{
		if (dirp->d_type == DT_REG)
		{
			stringstream ss(string(dirp->d_name));
			string item;
			vector<string> parts;
			while (getline(ss, item, '.'))
				parts.push_back(item);

			std::transform(parts[1].begin(), parts[1].end(), parts[1].begin(), ::toupper);

			if (parts[1] == "GS")
			{
				char fullFileLocation[256];
				sprintf(fullFileLocation, "%s%s%s", entryPoint, "/", dirp->d_name);
				string fullFileLocationString = string(fullFileLocation);
				luaFileNames[fullFileLocationString] = string(parts[0]);

			}

			if (parts[1] == "GC")
			{
				char fullFileLocation[256];
				sprintf(fullFileLocation, "%s%s%s", entryPoint, "/", dirp->d_name);
				string fullFileLocationString = string(fullFileLocation);
				glitchComponents[fullFileLocationString] = string(parts[0]);
			}

			if (parts[1] == "GP")
			{
				char fullFileLocation[256];
				sprintf(fullFileLocation, "%s%s%s", entryPoint, "/", dirp->d_name);
				string fullFileLocationString = string(fullFileLocation);
				glitchPrefabs.push_back(fullFileLocationString);
			}

			if (parts[1] == "FRAG")
			{
				char fullFileLocation[256];
				sprintf(fullFileLocation, "%s%s%s", entryPoint, "/", dirp->d_name);
				string fullFileLocationString = string(fullFileLocation);
				fullFileLocationString.resize(fullFileLocationString.length() - (parts[1].length() + 1));
				loader->LoadShader(parts[0], fullFileLocationString);
			}
		}
		else if (dirp->d_type == DT_DIR && string(dirp->d_name) != "." && string(dirp->d_name) != "..")
		{
			char newEntryPoint[256];
			sprintf(newEntryPoint, "%s%s%s", entryPoint, "/", dirp->d_name);
			IterateDirsUnix(newEntryPoint);
		}

		dirp = readdir(dp);
	}

	closedir(dp);
}
#endif

unsigned int ResourceManager::LoadWav(string filename)
{
	ALenum format;
	int channels, sampleRate, bps, size;
	char buffer[4];
	FILE *f = fopen(filename.c_str(), "rb");

	fread(buffer, sizeof(char), 4, f);
	if (strncmp(buffer, "RIFF", 4) != 0)
		Logger::Log("Not a Valid WAV File");

	fseek(f, ftell(f) + 12, SEEK_SET);
	fread(buffer, sizeof(char), 4, f);
	int fmtChunkSize = BinaryCharToInt(buffer, 4);

	fread(buffer, sizeof(char), 2, f);
	int isPCM = BinaryCharToInt(buffer, 2);

	fread(buffer, sizeof(char), 2, f);
	channels = BinaryCharToInt(buffer, 2);

	fread(buffer, sizeof(char), 4, f);
	sampleRate = BinaryCharToInt(buffer, 4);

	fseek(f, ftell(f) + 6, SEEK_SET);
	fread(buffer, sizeof(char), 2, f);
	bps = BinaryCharToInt(buffer, 2);

	fseek(f, 20 + fmtChunkSize, SEEK_SET);

	if (isPCM != 0x01)
		fseek(f, ftell(f) + 12, SEEK_SET);

	fread(buffer, sizeof(char), 4, f);
	if (strncmp(buffer, "PEAK", 4) == 0)
	{
		fread(buffer, sizeof(char), 4, f);
		int peakChunkSize = BinaryCharToInt(buffer, 4);
		fseek(f, ftell(f) + peakChunkSize + 4, SEEK_SET);
	}

	if (strncmp(buffer, "LIST", 4) == 0)
	{
		fread(buffer, sizeof(char), 4, f);
		int listChunkSize = BinaryCharToInt(buffer, 4);
		fseek(f, ftell(f) + listChunkSize + 4, SEEK_SET);
	}

	fread(buffer, sizeof(char), 4, f);
	size = BinaryCharToInt(buffer, 4);

	char* data = new char[size];
	fread(data, sizeof(char), size, f);
	fclose(f);

	if (channels == 2)
	{
		switch (bps)
		{
		case 8:
			format = AL_FORMAT_STEREO8;
			break;
		case 16:
			format = AL_FORMAT_STEREO16;
		}
	}
	else
	{
		switch (bps)
		{
		case 8:
			format = AL_FORMAT_MONO8;
			break;
		case 16:
			format = AL_FORMAT_MONO16;
			break;
		}
	}

	unsigned int bufferID;
	alGenBuffers(1, &bufferID);
	alBufferData(bufferID, format, data, size, sampleRate);
	return bufferID;
}

int ResourceManager::BinaryCharToInt(char *buff, int len)
{
	int ret = 0;

	//Reverse Byte Order
	if (isLittleEndian)
	{
		for (int i = 0; i < len; i++)
			((char*)&ret)[i] = buff[i];
	}
	else
	{
		for (int i = 0; i < len; i++)
			((char*)&ret)[3 - i] = buff[i];
	}
	return ret;
}

void ResourceManager::CheckForThread()
{
	if (atomic_load(&doneLoading) == 1)
	{
		LED->DispatchLoadingQueueEvents(functionReference, 1.f);
		atomic_store(&doneLoading, 0);
	}
}

void ResourceManager::LoadSound(string key, string path)
{
	if (queueIsOn)
	{
		FunctionParameters p;
		p.key = key;
		p.path = path;
		FunctionPointer pointer = make_pair(FunctionType::Sound, p);
		functionQueue.push_back(pointer);
		return;
	}

	stringstream ss(path);
	string item;
	vector<string> parts;
	while (getline(ss, item, '/'))
		parts.push_back(item);

	string fileNameExt = parts[parts.size() - 1];
	ss = stringstream(fileNameExt);
	parts.clear();
	while (getline(ss, item, '.'))
		parts.push_back(item);

	std::transform(parts[1].begin(), parts[1].end(), parts[1].begin(), ::toupper);

	if (parts[1] == "WAV")
	{
		SoundInfo s;
		s.bufferID = LoadWav("Resources/" + path);
		s.isOgg = false;

		sounds[key] = s;
	}

	if (parts[1] == "OGG")
	{
		SoundInfo s;
		alGenBuffers(QUEUEBUFFERS, s.queueBuffersIDs);
		s.isOgg = true;
		s.fileLocation = "Resources/" + path;
		sounds[key] = s;
	}
}

void ResourceManager::LoadFont(string key, string path)
{
	if (queueIsOn)
	{
		FunctionParameters p;
		p.key = key;
		p.path = path;
		FunctionPointer pointer = make_pair(FunctionType::Font, p);
		functionQueue.push_back(pointer);
		return;
	}

	stringstream ss(path);
	string item;
	vector<string> parts;
	while (getline(ss, item, '/'))
		parts.push_back(item);

	string fileNameExt = parts[parts.size() - 1];
	ss = stringstream(fileNameExt);
	parts.clear();
	while (getline(ss, item, '.'))
		parts.push_back(item);

	std::transform(parts[1].begin(), parts[1].end(), parts[1].begin(), ::toupper);

	if (parts[1] == "TTF")
	{
		string fullFileLocationString = "Resources/" + path;
		FT_New_Face(ft, fullFileLocationString.c_str(), 0, &faces[key]);
	}
}

void ResourceManager::LoadAnimation(string key, string path)
{
	if (queueIsOn)
	{
		FunctionParameters p;
		p.key = key;
		p.path = path;
		FunctionPointer pointer = make_pair(FunctionType::AnimationJSon, p);
		functionQueue.push_back(pointer);
		return;
	}

	stringstream ss(path);
	string item;
	vector<string> parts;
	while (getline(ss, item, '/'))
		parts.push_back(item);

	string fileNameExt = parts[parts.size() - 1];
	ss = stringstream(fileNameExt);
	parts.clear();
	while (getline(ss, item, '.'))
		parts.push_back(item);

	std::transform(parts[1].begin(), parts[1].end(), parts[1].begin(), ::toupper);

	if (parts[1] == "JSON")
	{
		string fullFileLocationString = "Resources/" + path;
		animJsonFileLocations[key] = fullFileLocationString;
	}
}

void ResourceManager::LoadTexture(string key, string path, unsigned int filterMode, unsigned int wrapMode)
{
	if (queueIsOn)
	{
		FunctionParameters p;
		p.key = key;
		p.path = path;
		p.filterMode = filterMode;
		p.wrapMode = wrapMode;
		FunctionPointer pointer = make_pair(FunctionType::Texture, p);
		functionQueue.push_back(pointer);
		return;
	}

	stringstream ss(path);
	string item;
	vector<string> parts;
	while (getline(ss, item, '/'))
		parts.push_back(item);

	string fileNameExt = parts[parts.size() - 1];
	ss = stringstream(fileNameExt);
	parts.clear();
	while (getline(ss, item, '.'))
		parts.push_back(item);

	string fullFileLocationString = "Resources/" + path;
	std::transform(parts[1].begin(), parts[1].end(), parts[1].begin(), ::toupper);
	if (parts[1] == "PNG" || parts[1] == "JPEG" || parts[1] == "TGA" || parts[1] == "BMP" || parts[1] == "JPG")
	{
		TextureInfo text;
		if (parts[1] == "PNG" || parts[1] == "TGA")
			text.format = ImageFormat::Transparent;
		else
			text.format = ImageFormat::Solid;

		if (text.format == ImageFormat::Transparent)
			text.image = stbi_load(fullFileLocationString.c_str(), &text.textW, &text.textH, &text.components, STBI_rgb_alpha);
		else
			text.image = stbi_load(fullFileLocationString.c_str(), &text.textW, &text.textH, &text.components, STBI_rgb);

		glGenTextures(1, &text.bufferID);
		glBindTexture(GL_TEXTURE_2D, text.bufferID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterMode);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterMode);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);

		if (text.format == ImageFormat::Transparent)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, text.textW, text.textH, 0, GL_RGBA, GL_UNSIGNED_BYTE, text.image);
		else
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, text.textW, text.textH, 0, GL_RGB, GL_UNSIGNED_BYTE, text.image);

		textures[key] = text;
	}
}

void ResourceManager::RegisterLoadingCallback()
{
	functionReference = LED->GetLuaFunctionReference();
}

void ResourceManager::EnebleLoadingQueue()
{
	queueIsOn = true;
}

void ResourceManager::ProcessLoadingQueue()
{
	queueIsOn = false;
	processThread = thread(&ResourceManager::ThreadLoading, this);
	processThread.detach();
}

void ResourceManager::ThreadLoading()
{
	WindowHandler* WH = &WindowHandler::GetInstance();
	SDL_GL_MakeCurrent(WH->gameWindow, WH->loadingContext);

	for (unsigned int i = 0; i < functionQueue.size(); i++)
	{
		FunctionPointer func = functionQueue[i];
		switch (func.first)
		{
		case FunctionType::Sound:
			LoadSound(func.second.key, func.second.path);
			break;

		case FunctionType::Font:
			LoadFont(func.second.key, func.second.path);
			break;

		case FunctionType::AnimationJSon:
			LoadAnimation(func.second.key, func.second.path);
			break;

		case FunctionType::Texture:
			LoadTexture(func.second.key, func.second.path, func.second.filterMode, func.second.wrapMode);
			break;
		}

		LED->DispatchLoadingQueueEvents(functionReference, (float)i / (float)functionQueue.size());
	}

	GLsync fenceId = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);

	while (true)
	{
		GLenum result = glClientWaitSync(fenceId, GL_SYNC_FLUSH_COMMANDS_BIT, GLuint64(5000000000));
		if (result != GL_TIMEOUT_EXPIRED) break;
	}

	atomic_store(&doneLoading, 1);
	SDL_GL_MakeCurrent(WH->gameWindow, NULL);
	functionQueue.clear();
	return;
}