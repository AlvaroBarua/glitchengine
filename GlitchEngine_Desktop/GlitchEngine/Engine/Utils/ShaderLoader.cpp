#include <Utils/ShaderLoader.h>
#include <Objects/GameObject.h>

ShaderLoader::ShaderLoader()
{
	quadVerts = { -0.5f, 0.5f, 0.f, 0.f, 0.f, -0.5f, -0.5f, 0.f, 0.f, 1.f, 0.5f, -0.5f, 0.f, 1.f, 1.f, 0.5f, 0.5f, 0.f, 1.f, 0.f };

	string GE_COLOR_VERT = "#version 330\n #(color,vec4); in vec3 vertexPosition; uniform mat4 ViewProj; in mat4 Model; in vec4 color; flat out vec4 Color; void main() { gl_Position = (ViewProj * Model) * vec4(vertexPosition, 1.0); Color = color; }";
	string GE_COLOR_FRAG = "#version 330\n flat in vec4 Color; out vec4 fragColor; void main() { fragColor = Color; }";

	string GE_TEXTURE_VERT = "#version 330\n #(tex,tex); #(flip,vec2); in vec3 vertexPosition; in vec2 textureCoords; in vec2 flip; uniform mat4 ViewProj; in mat4 Model; out vec2 UV; out vec2 Flip; void main() { Flip = flip; UV = textureCoords; gl_Position = (ViewProj * Model) * vec4(vertexPosition, 1.0); }";
	string GE_TEXTURE_FRAG = "#version 330\n in vec2 UV; in vec2 Flip; uniform sampler2D texture; out vec4 fragColor; void main() { vec2 UV2 = UV; if(Flip.x == 1)UV2.x = 1 - UV.x; if(Flip.y == 1)UV2.y = 1 - UV.y; vec4 texture = texture2D(texture, UV2); fragColor = texture; }";

	string GE_SPRITED_VERT = "#version 330\n #(tex,tex); #(texel,vec2); #(frame,vec4); #(flip,vec2); in vec3 vertexPosition; in vec2 textureCoords; in vec2 texel; in vec2 flip; in vec4 frame; uniform mat4 ViewProj; in mat4 Model; out vec2 UV; out vec2 Flip; flat out vec2 Texel; flat out vec4 Frame; void main() { Flip = flip; Texel = texel; Frame = frame; UV = textureCoords; gl_Position = (ViewProj * Model) * vec4(vertexPosition, 1.0); }";
	string GE_SPRITED_FRAG = "#version 330\n in vec2 UV; in vec2 Flip; uniform sampler2D texture; flat in vec2 Texel; flat in vec4 Frame; out vec4 fragColor; void main() { vec2 UV2 = UV; if(Flip.x == 1)UV2.x = 1 - UV.x; if(Flip.y == 1)UV2.y = 1 - UV.y; vec2 newUV = vec2(Texel.x * Frame.x, Texel.y * Frame.y) + vec2(UV2.x * Texel.x * Frame.z, UV2.y * Texel.y * Frame.w); vec4 texture = texture2D(texture, newUV); fragColor = texture; }";

	string GE_MOVIE_VERT = "#version 330\n #(tex,tex); in vec3 vertexPosition; in vec2 textureCoords; uniform mat4 ViewProj; in mat4 Model; out vec2 UV; void main() { UV = textureCoords; gl_Position = (ViewProj * Model) * vec4(vertexPosition, 1.0); }";
	string GE_MOVIE_FRAG = "#version 330\n in vec2 UV; uniform sampler2D planeY; uniform sampler2D planeU; uniform sampler2D planeV; out vec4 fragColor; void main() { highp float y = texture2D(planeY, UV).r; highp float u = texture2D(planeU, UV).r - 0.5; highp float v = texture2D(planeV, UV).r - 0.5; highp float r = y + 1.402 * v; highp float g = y - 0.344 * u - 0.714 * v; highp float b = y + 1.772 * u; fragColor = vec4(r, g, b, 1.0); }";

	string GE_FONT_VERT = "#version 330\n in vec3 vertexPosition; in vec2 textureCoords; uniform mat4 MVP; out vec2 UV; void main() { UV = textureCoords; gl_Position = MVP * vec4(vertexPosition.xy, 0.0, 1.0); }";
	string GE_FONT_FRAG = "#version 330\n in vec2 UV; uniform vec3 Color; uniform sampler2D texture; out vec4 fragColor; void main(void) { vec4 texture = texture2D(texture, UV); fragColor = vec4(Color, texture.a); }";

	string GE_BOTTON_TEXTURE_VERT = "#version 330\n #(tex,tex); in vec3 vertexPosition; in vec2 textureCoords; uniform mat4 MVP; out vec2 UV; void main() { UV = textureCoords; gl_Position = MVP * vec4(vertexPosition.xy, 0.0, 1.0); }";
	string GE_BOTTON_TEXTURE_FRAG = "#version 330\n in vec2 UV; uniform sampler2D texture; out vec4 fragColor; void main() { vec4 texture = texture2D(texture, UV); fragColor = texture; }";

	string GE_LIGHSCREEN_VERT = "#version 330\n in vec3 vertexPosition; uniform mat4 Model; uniform mat4 ViewProj; out vec2 worldPos; void main() { vec4 mWorldPos = Model * vec4(vertexPosition, 1.0); worldPos = vec2(mWorldPos.x, mWorldPos.y); gl_Position = (ViewProj * Model) * vec4(vertexPosition, 1.0); }";
	string GE_LIGHTSCREEN_FRAG = "#version 330\n struct LightSource { vec2 lightPos; float radious; vec3 color; float brightness; }; uniform vec3 ambientColor; in vec2 worldPos; uniform LightSource sources[%]; uniform int numberOfLights; out vec4 fragColor; void main() { vec3 colorAccum = vec3(0, 0, 0);	for (int i = 0; i < numberOfLights; i++) { float intensity = clamp(1.0 - distance(worldPos, sources[i].lightPos) / sources[i].radious, 0.0f, 1.0f);	colorAccum += sources[i].color * intensity * sources[i].brightness; } colorAccum = clamp(colorAccum, 0.0f, 1.0f); fragColor = vec4(colorAccum + ambientColor, 1); }";

	size_t index;
	while ((index = GE_LIGHTSCREEN_FRAG.find("%")) != string::npos)
		GE_LIGHTSCREEN_FRAG.replace(index, 1, std::to_string((&ApplicationData::GetInstance())->maxLightsOnScreen));

	glGenBuffers(1, &VertexBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, VertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * quadVerts.size(), &quadVerts[0], GL_STATIC_DRAW);

	LoadShader("GE_Color", GE_COLOR_VERT, GE_COLOR_FRAG);
	LoadShader("GE_Textured", GE_TEXTURE_VERT, GE_TEXTURE_FRAG);
	LoadShader("GE_Sprited", GE_SPRITED_VERT, GE_SPRITED_FRAG);
	LoadShader("GE_Movie", GE_MOVIE_VERT, GE_MOVIE_FRAG);
	LoadShader("GE_Font", GE_FONT_VERT, GE_FONT_FRAG);
	LoadShader("GE_Botton", GE_BOTTON_TEXTURE_VERT, GE_BOTTON_TEXTURE_FRAG);
	LoadShader("GE_LightScreen", GE_LIGHSCREEN_VERT, GE_LIGHTSCREEN_FRAG.c_str());
}

ShaderLoader::~ShaderLoader(void)
{
	for (auto const it : ProgramIDs)
		glDeleteProgram(it.second);

	ProgramIDs.clear();
}

GLuint ShaderLoader::CompileShader(GLenum mode, const char* source){
	GLuint shaderID = glCreateShader(mode);

	glShaderSource(shaderID, 1, &source, NULL);
	glCompileShader(shaderID);

	return shaderID;
}

string ShaderLoader::ReadFromFile(const char* file){
	FILE *f = fopen(file, "rt");
	fseek(f, 0, SEEK_END);

	int count = (int)ftell(f);

	rewind(f);

	char *data = (char*)malloc(sizeof(char)*(count + 1));
	count = (int)fread(data, sizeof(char), count, f);

	data[count] = '\0';
	fclose(f);

	return string(data);
}

void ShaderLoader::LoadShader(string shaderName, string fullFileLocation){

	GLuint programID = glCreateProgram();
	ProgramIDs[shaderName] = programID;

	char vertLocation[256];
	sprintf(vertLocation, "%s%s", fullFileLocation.c_str(), ".vert");
	string vertexS = ReadFromFile(const_cast<char*>(vertLocation));

	char fragLocation[256];
	sprintf(fragLocation, "%s%s", fullFileLocation.c_str(), ".frag");
	string fragmentS = ReadFromFile(const_cast<char*>(fragLocation));

	ShaderAttributes[shaderName] = ExtractAttributes(vertexS);
	GLuint vertex = CompileShader(GL_VERTEX_SHADER, vertexS.c_str());
	GLuint fragment = CompileShader(GL_FRAGMENT_SHADER, fragmentS.c_str());

	glAttachShader(programID, vertex);
	glAttachShader(programID, fragment);

	glBindAttribLocation(programID, 0, "vertexPosition");
	glBindAttribLocation(programID, 1, "textureCoords");
	glBindAttribLocation(programID, 2, "Model");

	glLinkProgram(programID);
	cameraUniformLocations[programID] = glGetUniformLocation(programID, "ViewProj");

	glDeleteShader(vertex);
	glDeleteShader(fragment);

	PrintShaderInfo(programID);
}

void ShaderLoader::LoadShader(string shaderName, string vert, string frag){

	GLuint programID = glCreateProgram();
	ProgramIDs[shaderName] = programID;

	ShaderAttributes[shaderName] = ExtractAttributes(vert);
	GLuint vertex = CompileShader(GL_VERTEX_SHADER, vert.c_str());
	GLuint fragment = CompileShader(GL_FRAGMENT_SHADER, frag.c_str());

	glAttachShader(programID, vertex);
	glAttachShader(programID, fragment);

	glBindAttribLocation(programID, 0, "vertexPosition");
	glBindAttribLocation(programID, 1, "textureCoords");
	glBindAttribLocation(programID, 2, "Model");

	glLinkProgram(programID);
	cameraUniformLocations[programID] = glGetUniformLocation(programID, "ViewProj");

	glDeleteShader(vertex);
	glDeleteShader(fragment);

	PrintShaderInfo(programID);
}

map<string, string> ShaderLoader::ExtractAttributes(string& shaderVert)
{
	cmatch cm;
	regex attributeDeclaration("#\\(\\s*([\\S]+)\\s*,\\s*([\\S]+)\\s*\\);\\s");
	map<string, string> attributes;

	auto words_begin = std::sregex_iterator(shaderVert.begin(), shaderVert.end(), attributeDeclaration);
	auto words_end = std::sregex_iterator();

	for (; words_begin != words_end; words_begin++)
		if (regex_search(words_begin->str().c_str(), cm, attributeDeclaration))
			attributes[cm[1].str()] = cm[2].str();

	shaderVert = regex_replace(shaderVert, attributeDeclaration, "");
	return attributes;
}

void ShaderLoader::PrintInfoLog(GLuint programme)
{
	int max_length = 2048;
	int actual_length = 0;
	char log[2048];
	glGetProgramInfoLog(programme, max_length, &actual_length, log);
	Logger::Log("Program info log for GL index %u:%s", programme, log);
}

void ShaderLoader::PrintShaderInfo(GLuint programme)
{
	int params = -1;
	glGetProgramiv(programme, GL_LINK_STATUS, &params);

	if (params == 0)
	{
		Logger::Log("-------------------- shader programme %i info:", programme);
		Logger::Log("GL_LINK_STATUS = %i", params);

		glGetProgramiv(programme, GL_ATTACHED_SHADERS, &params);
		Logger::Log("GL_ATTACHED_SHADERS = %i", params);

		glGetProgramiv(programme, GL_ACTIVE_ATTRIBUTES, &params);
		Logger::Log("GL_ACTIVE_ATTRIBUTES = %i", params);

		for (int i = 0; i < params; i++)
		{
			char name[64];
			int max_length = 64;
			int actual_length = 0;
			int size = 0;
			GLenum type;
			glGetActiveAttrib(programme, i, max_length, &actual_length, &size, &type, name);

			if (size > 1)
			{
				for (int j = 0; j < size; j++)
				{
					char long_name[64];
					sprintf(long_name, "%s[%i]", name, j);
					int location = glGetAttribLocation(programme, long_name);
					Logger::Log("  %i) type:%s name:%s location:%i", i, GLTypeToString(type), long_name, location);
				}
			}
			else
			{
				int location = glGetAttribLocation(programme, name);
				Logger::Log("  %i) type:%s name:%s location:%i", i, GLTypeToString(type), name, location);
			}
		}

		glGetProgramiv(programme, GL_ACTIVE_UNIFORMS, &params);
		Logger::Log("GL_ACTIVE_UNIFORMS = %i", params);
		for (int i = 0; i < params; i++)
		{
			char name[64];
			int max_length = 64;
			int actual_length = 0;
			int size = 0;
			GLenum type;
			glGetActiveUniform(programme, i, max_length, &actual_length, &size, &type, name);
			if (size > 1) 
			{
				for (int j = 0; j < size; j++) 
				{
					char long_name[64];
					sprintf(long_name, "%s[%i]", name, j);
					int location = glGetUniformLocation(programme, long_name);
					Logger::Log("  %i) type:%s name:%s location:%i", i, GLTypeToString(type), long_name, location);
				}
			}
			else 
			{
				int location = glGetUniformLocation(programme, name);
				Logger::Log("  %i) type:%s name:%s location:%i", i, GLTypeToString(type), name, location);
			}
		}

		PrintInfoLog(programme);
	}
}

const char* ShaderLoader::GLTypeToString(GLenum type) 
{
	switch (type) 
	{
		case GL_BOOL: return "bool";
		case GL_INT: return "int";
		case GL_FLOAT: return "float";
		case GL_FLOAT_VEC2: return "vec2";
		case GL_FLOAT_VEC3: return "vec3";
		case GL_FLOAT_VEC4: return "vec4";
		case GL_FLOAT_MAT2: return "mat2";
		case GL_FLOAT_MAT3: return "mat3";
		case GL_FLOAT_MAT4: return "mat4";
		case GL_SAMPLER_2D: return "sampler2D";
		case GL_SAMPLER_3D: return "sampler3D";
		case GL_SAMPLER_CUBE: return "samplerCube";
		case GL_SAMPLER_2D_SHADOW: return "sampler2DShadow";
		default: break;
	}
	return "other";
}