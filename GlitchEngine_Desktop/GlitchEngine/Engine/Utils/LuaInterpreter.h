#pragma once
#define lua_SetConst(state, name, val) { lua_pushnumber( state, name); lua_setglobal(state, val); }
#define lua_PrintError(state, section) { luaL_traceback(state, state, section, 1); Logger::Log("%s", lua_tostring(state, -1)); lua_pop(state, 1); Logger::Log("%s", lua_tostring(state, -1)); }

#include <SDL.h>
#include <lua.hpp>
#include <vector>
#include <map>
#include <string>
#include <glm/glm.hpp>
#include <LuaBridge/LuaBridge.h>

#include <Classes/Material.h>
#include <Classes/Transform.h>
#include <Classes/SpriteAnimator.h>
#include <Classes/LightScreen.h>
#include <Classes/PhysicsWorld.h>

#include <Objects/GameObject.h>
#include <Objects/Quad.h>
#include <Objects/Image.h>
#include <Objects/Sprite.h>
#include <Objects/MovieQuad.h>
#include <Objects/Camera.h>
#include <Objects/SoundListener.h>
#include <Objects/SoundManager.h>
#include <Objects/Gui.h>
#include <Objects/FrameBuffer.h>

#include <Utils/Logger.h>
#include <Utils/Application.h>
#include <Utils/TiledMapParser.h>
#include <Utils/TimeHandle.h>
#include <Utils/ResourceManager.h>
#include <Utils/Vector.h>
#include <Utils/FiniteStateMachine.h>
#include <Utils/LuaEventDispatcher.h>
#include <Utils/JSonParser.h>
#include <Utils/TimeHandle.h>

using namespace std;

class LuaInterpreter
{
private:
    lua_State* luaState;
	vector<string> fileNames;
	vector<string> awakeFiles;
	vector<string> updateFiles;
	vector<string> onGuiFiles;
    void RegisterConstants();
	void RegisterClasses();

	ResourceManager *RM;
	LuaEventDispatcher *LED;
	Gui *gui;
	ApplicationData *appData;
	TimeHandle *timeHandle;

public:
	LuaInterpreter();
	~LuaInterpreter();
	void ProcessFiles();
	void ProcessGUI();
	bool endExecution;
};