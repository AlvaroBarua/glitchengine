#include <Utils/TimeHandle.h>

TimeHandle::TimeHandle()
{
	time = 0;
	delta = 0;
}

TimeHandle::~TimeHandle()
{

}

void TimeHandle::SetTime(double counter)
{
	delta = counter / (double)1000000000;
	time += delta;
}

double TimeHandle::GetTime() const
{
	return time;
}

double TimeHandle::GetDelta() const
{
	return delta;
}

TimeHandle &TimeHandle::GetInstance()
{

	static TimeHandle *time = NULL;

	if (time == NULL)
		time = new TimeHandle();

	return *time;
}

void TimeHandle::Destroy()
{

	TimeHandle *time = &GetInstance();
	if (time != NULL)
		delete time;
}
