#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <glm/glm.hpp>
#include <json/json.h>
#include <Utils/Vector.h>

class JSonParser
{
    public:
        JSonParser(const char* fileLocation);
        ~JSonParser();
        int GetInt(const char* path);
		int GetFloat(const char* path);
        std::string GetString(const char* path);
		bool GetBool(const char* path);
		Vector<glm::vec2> GetVec2(const char* path);
		Vector<glm::vec3> GetVec3(const char* path);
		Vector<glm::vec4> GetVec4(const char* path);

    private:
		Json::Value root;
		Json::Value GetField(const char* path);
};