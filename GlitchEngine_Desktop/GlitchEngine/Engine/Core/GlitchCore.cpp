#include "GlitchCore.h"

/************* Static Initialization *************/
map<GLuint, int> Renderer::RenderableObjects;
map<GLuint, map<string, GLuint>> Renderer::RenderableBuffers;
map<GLuint, vector<int>> Renderer::AvailableSpaces;
map<GLuint, GLuint> Renderer::VertexArraysID;
vector<Material*> Renderer::MaterialUpdates;
vector<GameObject*> Renderer::ObjectsWithRigidBody;
vector<GameObject*> Renderer::TransformUpdates;
vector<GameObject*> Renderer::ComponentsUpdate;
vector<GameObject*> Renderer::ComponentsToAddAfterUpdate;
vector<GameObject*> Renderer::ComponentsToDeleteAfterUpdate;
map<GLuint, vector<int>> Renderer::ShaderVertexRelation;
map<GLuint, GLuint> Renderer::VertexShaderRelation;
map<GLuint, map<GLuint, GLuint>> Renderer::ShaderTextureVertexRelation;
map<GLuint, map<string, GLuint>> Renderer::VertexTexturesRelation;

int Renderer::ObjectUniqueID;
vector<SoundManager::SoundSource*> SoundManager::ProcessingList;
vector<Camera*> Camera::CamList;
map<string, GLuint> ShaderLoader::ProgramIDs;
map<GLuint, GLint> ShaderLoader::cameraUniformLocations;
map<string, map<string, string>> ShaderLoader::ShaderAttributes;
GLuint ShaderLoader::VertexBufferID;
vector<MovieQuad*> MovieQuadManager::quadList;

/************* Static Initialization *************/

GlitchCore::GlitchCore()
{
	#if NDEBUG
		#if WIN32
			ShowWindow(GetConsoleWindow(), SW_HIDE);
		#endif
		Logger::RedirectToFile();
	#endif

	listener = new ContactListener();
	debugDraw = new PhysicsDebugDraw();
	filtering = new b2ContactFilter();
	windowHandler = &WindowHandler::GetInstance();
	appData = &ApplicationData::GetInstance();
	timeHandle = &TimeHandle::GetInstance();
	pWorld = &PhysicsWorld::GetInstance();
	input = &Input::GetInstance();
}

GlitchCore::~GlitchCore()
{
}

bool GlitchCore::Initialize()
{
	config = new JSonParser("config.json");

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)
	{
		Logger::Log("SDL could not initialize! SDL_Error: %s", SDL_GetError());
		return false;
	}
	else
	{
		appData->renderWidth = config->GetInt("displayMode.renderWidth");
		appData->renderHeight = config->GetInt("displayMode.renderHeight");
		appData->maxLightsOnScreen = config->GetInt("lightning.maxLightsOnScreen");
		appData->useLights = config->GetBool("lightning.useLights");
		appData->ambientColor = config->GetVec3("lightning.ambientColor");
		appData->designWidth = config->GetInt("displayMode.designWidth");
		appData->designHeight = config->GetInt("displayMode.designHeight");

		Uint32 windowFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;
		if (config->GetBool("displayMode.fullScreen"))
		{
			windowFlags |= SDL_WINDOW_FULLSCREEN;
			SDL_DisplayMode dm;
			if (SDL_GetDesktopDisplayMode(0, &dm) != 0)
			{
				Logger::Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());
				return false;
			}

			appData->renderWidth = dm.w;
			appData->renderHeight = dm.h;
		}

		windowHandler->gameWindow = SDL_CreateWindow(config->GetString("displayMode.windowTittle").c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			appData->renderWidth, appData->renderHeight, windowFlags);

		if (windowHandler->gameWindow == NULL)
		{
			
			Logger::Log("Window could not be created! SDL_Error: %s", SDL_GetError());
			return false;
		}
		else
		{
			SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
			windowHandler->loadingContext = SDL_GL_CreateContext(windowHandler->gameWindow);
			windowHandler->renderContext = SDL_GL_CreateContext(windowHandler->gameWindow);
			SDL_GL_MakeCurrent(windowHandler->gameWindow, windowHandler->renderContext);

			if (windowHandler->loadingContext == NULL || windowHandler->renderContext == NULL)
			{
				Logger::Log("OpenGL context could not be created! SDL Error: %s", SDL_GetError());
				return false;
			}

			if (config->GetBool("displayMode.verticalSync"))
			{
				int result = SDL_GL_SetSwapInterval(1);
				if (result == -1)
					Logger::Log("Could not set Vertical Sync, using target FPS -- SDL_Error: %s", SDL_GetError());
				else
					vSync = true;
			}
		}
	}

	Logger::Log("OpenGL Version: %s", glGetString(GL_VERSION));
	Logger::Log("OpenGL Vendor: %s", glGetString(GL_VENDOR));

	glewExperimental = GL_TRUE;
	glewInit();

	oalDevice = alcOpenDevice(NULL);
	if (oalDevice == NULL)
	{
		Logger::Log("Error Opening Sund Card");
		return false;
	}

	oalContext = alcCreateContext(oalDevice, NULL);
	if (oalContext == NULL)
	{
		Logger::Log("Error Creating OpenAl Context");
		return false;
	}

	alcMakeContextCurrent(oalContext);
	SoundListener::SetOrientation();

	RM = &ResourceManager::GetInstance();
	spriteAnimator = &SpriteAnimator::GetInstance();
	camera = new Camera(Vector<glm::vec2>(0.f, 0.f), true);
	lightScreen = &LightScreen::GetInstance();

	if (config->GetBool("displayMode.preserveAspectRatio"))
	{
		appData->targetAspectRatio = (float)appData->designWidth / (float)appData->designHeight;
		appData->currentAspectRatio = (float)appData->renderWidth / (float)appData->renderHeight;

		float width = (float)appData->renderWidth;
		float height = (float)((int)(width / appData->targetAspectRatio));

		if (height > appData->renderHeight)
		{
			height = (float)appData->renderHeight;
			width = (float)((int)(height * appData->targetAspectRatio));
		}

		float vp_x = (appData->renderWidth / 2) - (width / 2);
		float vp_y = (appData->renderHeight / 2) - (height / 2);

		vp_x /= appData->renderWidth;
		width /= appData->renderWidth;
		vp_y /= appData->renderHeight;
		height /= appData->renderHeight;

		camera->SetViewport(Vector<glm::vec4>(vp_x, vp_y, width + vp_x, height + vp_y), true);
	}
	else
		camera->SetViewport(Vector<glm::vec4>(0, 0, 1, 1), false);

	fbo = &FrameBuffer::GetInstance();
	tiledParser = &TiledMapParser::GetInstance();

	pWorld->GetWorld()->SetContactListener(listener);
	pWorld->GetWorld()->SetContactFilter(filtering);
	pWorld->GetWorld()->SetDebugDraw(debugDraw);
	debugDraw->SetFlags(b2Draw::e_shapeBit);

	lua = new LuaInterpreter();
}

void GlitchCore::Run()
{
	if (lua->endExecution)
		return;

	lastTime = Clock::now();
	Vector<glm::vec3> clearColor = config->GetVec3("displayMode.clearColor");
	glClearColor((GLfloat)(clearColor.Get<0>()) / 255.0, (GLfloat)(clearColor.Get<1>()) / 255.0, (GLfloat)(clearColor.Get<2>()) / 255.0, 1.f);

	glEnable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0);
	glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	bool quit = false;
	long long fps = 1.0 / (double)config->GetInt("displayMode.targetFPS") * 1000000000;
	
	while (!input->PollEvents())
	{
		GetDelta();
		counter += timeHandle->rawDeltaTime;

		if (vSync)
			TimedLoop();
		else
			if (counter >= fps)
				TimedLoop();
	}
}

void GlitchCore::Terminate()
{
	input->Destroy();
	timeHandle->Destroy();
	spriteAnimator->Destroy();
	lightScreen->Destroy();
	appData->Destroy();
	fbo->Destroy();
	RM->Destroy();

	(&TiledMapParser::GetInstance())->Destroy();
	(&Gui::GetInstance())->Destroy();

	if(camera != nullptr)
		delete camera;
	
	if(config != nullptr)	
		delete config;
	
	if(filtering != nullptr)
		delete filtering;
	
	Renderer::DeleteBuffers();
	
	if(lua != nullptr)
		delete lua;
	
	pWorld->Destroy();
	if (listener != nullptr)
		delete listener;

	if (debugDraw != nullptr)
		delete debugDraw;

	(&LuaEventDispatcher::GetInstance())->Destroy();

	#if NDEBUG
		Logger::Terminate();
	#endif

	alcDestroyContext(oalContext);
	alcCloseDevice(oalDevice);
	SDL_GL_DeleteContext(windowHandler->loadingContext);
	SDL_GL_DeleteContext(windowHandler->renderContext);
	SDL_DestroyWindow(windowHandler->gameWindow);
	windowHandler->Destroy();
	SDL_Quit();
}

void GlitchCore::TimedLoop()
{
	timeHandle->SetTime(counter);

	spriteAnimator->UpdateAnimations(timeHandle->rawDeltaTime);
	MovieQuadManager::UpdateMovieQuads(timeHandle->rawDeltaTime);
	SoundManager::ProcessSourcesList();
	RM->CheckForThread();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (fbo->IsEnabled())
		fbo->Activate();

	pWorld->Step();
	lua->ProcessFiles();
	Renderer::UpdateRigidbodies();

	// -- Cameras and Lights
	for (auto const& it : Camera::CamList)
	{
		if (it->IsActive())
		{
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glm::vec4 vp = it->GetViewport();
			glViewport(GLint(vp.x), GLint(vp.y), GLint(vp.z), GLint(vp.w));
			Renderer::DrawAllObjects(it);

			if (appData->useLights)
			{
				glBlendFunc(GL_DST_COLOR, GL_ZERO);
				lightScreen->DrawScreen(it);
			}
		}
	}

	// -- PostProcessing
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	if (fbo->IsEnabled())
	{
		if (fbo->IsGuiIncluded())
		{
			lua->ProcessGUI();
			fbo->Deactivate();
		}
		else
		{
			fbo->Deactivate();
			lua->ProcessGUI();
		}
	}
	else
		lua->ProcessGUI();

#if _DEBUG || _DEVEL
	if (pWorld->GetDebugDraw())
	{
		debugDraw->InitDraw();
		pWorld->GetWorld()->DrawDebugData();
		debugDraw->EndDraw();
	}
#endif
	
	SDL_GL_SwapWindow(windowHandler->gameWindow);
	counter = 0;
}

void GlitchCore::GetDelta()
{
	std::chrono::duration<long long, std::nano> delta;
	std::chrono::high_resolution_clock::time_point currentTime;

	currentTime = Clock::now();
	delta = currentTime - lastTime;

	timeHandle->rawDeltaTime = delta.count();
	lastTime = currentTime;
}