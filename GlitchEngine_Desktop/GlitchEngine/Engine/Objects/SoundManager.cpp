#include <Objects/SoundManager.h>


SoundManager::SoundManager()
{
}


SoundManager::~SoundManager()
{
}

SoundManager::SoundSource::SoundSource(string name)
{
	soundName = name;
	RM = &ResourceManager::GetInstance();
	alGenSources(1, &sourceID);
	currentSound = RM->sounds[name];

	if (currentSound.isOgg)
	{
		p = fopen(currentSound.fileLocation.c_str(), "rb");
		if (ov_open_callbacks(p, &vf, NULL, 0, OV_CALLBACKS_NOCLOSE) < 0)
		{
			Logger::Log("SoundSource Error: Input does not appear to be an Ogg bitstream.");
			return;
		}

		vi = ov_info(&vf, -1);

		if (vi->channels == 1)
		{
			format = AL_FORMAT_MONO16;
			buffSize = vi->rate >> 1;
			buffSize -= (buffSize % 2);
		}
		else if (vi->channels == 2)
		{
			format = AL_FORMAT_STEREO16;
			buffSize = vi->rate;
			buffSize -= (buffSize % 4);
		}

		decodedBuffer = new char[buffSize];

		for (int i = 0; i < QUEUEBUFFERS; i++)
		{
			long bytesRead = DecodeOGG();
			if (bytesRead)
			{
				alBufferData(currentSound.queueBuffersIDs[i], format, decodedBuffer, bytesRead, vi->rate);
				alSourceQueueBuffers(sourceID, 1, &currentSound.queueBuffersIDs[i]);
			}
		}
	}
	else
		alSourcei(sourceID, AL_BUFFER, currentSound.bufferID);

}


SoundManager::SoundSource::~SoundSource()
{
	SoundManager::RemoveFromSourceProcessingList(this);

	if (currentSound.isOgg)
	{
		ov_clear(&vf);
		fclose(p);
	}

	alDeleteSources(1, &sourceID);
}

void SoundManager::SoundSource::Play(bool _loop)
{
	ALenum state;
	alGetSourcei(sourceID, AL_SOURCE_STATE, &state);

	if (!currentSound.isOgg)
		alSourcei(sourceID, AL_LOOPING, _loop);
	else
	{
		loop = _loop;
		SoundManager::AddToSourceProcessingList(this);
	}

	if (state != AL_PLAYING)
		alSourcePlay(sourceID);
	else
	{
		Stop();
		Play(_loop);
	}
}

void SoundManager::SoundSource::Stop()
{
	alSourceStop(sourceID);
}

void SoundManager::SoundSource::Pause()
{
	alSourcePause(sourceID);
}

void SoundManager::SoundSource::ReSet(bool loop)
{
	Stop();
	Play(loop);
}

void SoundManager::SoundSource::SetPosition(Vector<glm::vec2> pos)
{
	alSource3f(sourceID, AL_POSITION, pos.Get<0>(), pos.Get<1>(), 0);
}

void SoundManager::SoundSource::SetVelocity(Vector<glm::vec2> vel)
{
	alSource3f(sourceID, AL_VELOCITY, vel.Get<0>(), vel.Get<1>(), 0);
}

void SoundManager::SoundSource::SetVolume(float gain)
{
	alSourcef(sourceID, AL_GAIN, gain);
}

void SoundManager::SoundSource::SetRollOffFactor(float factor)
{
	alSourcef(sourceID, AL_ROLLOFF_FACTOR, factor);
}

void SoundManager::SoundSource::SetPitch(float pitch)
{
	alSourcef(sourceID, AL_PITCH, pitch);
}

void SoundManager::SoundSource::SetReferenceDistance(float dist)
{
	alSourcef(sourceID, AL_REFERENCE_DISTANCE, dist);
}

void SoundManager::SoundSource::SetMaxDistance(float dist)
{
	alSourcef(sourceID, AL_MAX_DISTANCE, dist);
}

unsigned long SoundManager::SoundSource::DecodeOGG()
{
	int current_section;
	unsigned long bytesRead = 0;

	while (1)
	{
		long decodedSize = ov_read(&vf, decodedBuffer + bytesRead, buffSize - bytesRead, (int)(!RM->isLittleEndian), 2, 1, &current_section);
		if (decodedSize > 0)
		{
			bytesRead += decodedSize;

			if (bytesRead >= buffSize)
				break;
		}
		else
		{
			break;
		}
	}

	return bytesRead;
}

void SoundManager::SoundSource::ProcessStream()
{
	int processedBuffers = 0;
	alGetSourcei(sourceID, AL_BUFFERS_PROCESSED, &processedBuffers);
	while (processedBuffers)
	{
		unsigned int dequeuedBufferID = 0;
		alSourceUnqueueBuffers(sourceID, 1, &dequeuedBufferID);

		long bytesRead = DecodeOGG();
		if (bytesRead)
		{
			alBufferData(dequeuedBufferID, format, decodedBuffer, bytesRead, vi->rate);
			alSourceQueueBuffers(sourceID, 1, &dequeuedBufferID);
		}

		processedBuffers--;
	}

	ALenum state;
	alGetSourcei(sourceID, AL_SOURCE_STATE, &state);
	if (state != AL_PLAYING)
	{
		ALint queuedBuffers;
		alGetSourcei(sourceID, AL_BUFFERS_QUEUED, &queuedBuffers);
		if (queuedBuffers)
		{
			alSourcePlay(sourceID);
		}
		else
		{
			if (loop)
			{
				fseek(p, 0, SEEK_SET);
				ov_pcm_seek(&vf, 0);
				PlayVorbisLoop();
			}
			else
			{
				SoundManager::RemoveFromSourceProcessingList(this);
			}
		}
	}
}

void SoundManager::SoundSource::PlayVorbisLoop()
{
	for (int i = 0; i < QUEUEBUFFERS; i++)
	{
		long bytesRead = DecodeOGG();
		if (bytesRead)
		{
			alBufferData(currentSound.queueBuffersIDs[i], format, decodedBuffer, bytesRead, vi->rate);
			alSourceQueueBuffers(sourceID, 1, &currentSound.queueBuffersIDs[i]);
		}
	}
}

