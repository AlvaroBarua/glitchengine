#include "Image.h"

Image::Image()
{
}

Image::Image(Vector<glm::vec2> position, float rotation, Vector<glm::vec2> size, short sLayer, bool translucent, string image)
{
	material = new Material("GE_Textured", translucent);
	material->SetTexture("texture", image);

	transform->Translate(position, Space::WorldSpace);
	transform->Rotate(rotation);
	transform->ScaleObject(size);
	transform->SetLayer(sLayer);

	if (material != nullptr)
	{
		int arrayPos = material->GetArrayPosition();
		if (arrayPos != -1)
		{
			AddToRendererList(arrayPos);
			material->AddParentPosition(positionInGroup);
		}
		else
		{
			Logger::Log("Quad Error!: Make sure to set Material textures before initializing.");
			delete this;
		}
	}
	else
	{
		Logger::Log("Quad Error!: You are passing a null Material value.");
		delete this;
	}
}

Image::~Image()
{
}

void Image::FlipX(bool flipped)
{
	material->SetVec2("flip", Vector<glm::vec2>(flipped == true ? 1 : 0, 0));
}

void Image::FlipY(bool flipped)
{
	material->SetVec2("flip", Vector<glm::vec2>(0, flipped == true ? 1 : 0));
}
