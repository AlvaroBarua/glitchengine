#pragma once
#define MAX_QUEUE_BUFFERS 8
#define MAX_TEXTURE_UNITS 8

#include <Objects/Quad.h>
#include <Classes/Renderer.h>
#include <Utils/ThreadUtils.h>

#include <thread>
#include <mutex>

#include <mkvparser.hpp>
#include <mkvreader.hpp>
#include <vorbis/codec.h>
#include <ogg/ogg.h>
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>
#include <vpx_decoder.h>
#include <vp8dx.h>

using namespace mkvparser;

struct VideoData
{
	unsigned char* planeY;
	unsigned char* planeU;
	unsigned char* planeV;

	unsigned char* operator[] (int key)
	{
		switch (key)
		{
		case 0:
			return planeY;
			break;
		case 1:
			return planeU;
			break;
		case 2:
			return planeV;
			break;
		}
	}
};

class MovieQuadManager;
class MovieQuad : public Quad
{
private:
	int h;
	int w;
	int hW;
	int hH;

	long long fps;
	GLuint texBufferIDs[3];
	string fileLocation;
	Material* material;

	atomic_bool audioParseFinished;
	atomic_bool videoParseFinished;
	bool genNextFrame = false;

	// -- vpx
	bool vpxSetup = false;
	vpx_codec_ctx_t	vpxDecoder;
	SafeQueue<VideoData*> videoDataQueue;

	// -- Vorbis
	bool vorbisSetup = false;
	vorbis_info* vi = new vorbis_info();
	vorbis_comment* vc = new vorbis_comment();
	vorbis_dsp_state* vd = new vorbis_dsp_state();
	vorbis_block* vb = new vorbis_block();

	// -- OpenAL
	ALenum format;
	ALuint bufferIDs[MAX_QUEUE_BUFFERS];
	ALuint sourceID;
	SafeQueue< vector<short>> audioDataQueue;
	vector<short> blockBuffer;
	size_t blockBufferSize = 8192;
	bool bufferGenerated = false;

	// -- Threading
	thread audioThread;
	thread videoThread;
	bool stop;
	atomic_bool exitThread;
	bool hasInit;
	
	// -- Delta
	long long counter = 0;
	bool audioStarted = false;

	void VideoParse();
	void AudioParse();
	void GnerateTextures();
	void GenerateTextureBuffers();

public:
	MovieQuad(Vector<glm::vec2> position, float rotation, Vector<glm::vec2> size, short sLayer, string file, float fps);
	~MovieQuad();

	void Play();
	void Stop();
	bool GetStatus();
	bool HasStartedPlaying();
	void MovieUpdateLoop(double deltaTime);
};

