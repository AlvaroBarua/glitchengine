#pragma once
#include <Objects\Image.h>

class Sprite : public Image
{
public:
	Sprite();
	Sprite(Vector<glm::vec2> position, float rotation, Vector<glm::vec2> size, short sLayer, bool translucent, string image);
	~Sprite();

	void SetTexelSize(Vector<glm::vec2> size);
	void SetFrame(Vector<glm::vec4> frame);
};
