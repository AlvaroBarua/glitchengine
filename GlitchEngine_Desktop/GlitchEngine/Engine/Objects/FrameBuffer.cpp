#include "FrameBuffer.h"

FrameBuffer &FrameBuffer::GetInstance()
{
	static FrameBuffer* ins;
	if (ins == NULL)
		ins = new FrameBuffer();
	return *ins;
}

void FrameBuffer::Destroy()
{
	delete &GetInstance();
}

FrameBuffer::FrameBuffer()
{
	isEnabled = false;
	includeGUI = false;
	appData = &ApplicationData::GetInstance();
	RM = &ResourceManager::GetInstance();

	Vector<glm::vec2> finalSize = Vector<glm::vec2>(appData->renderWidth, appData->renderHeight);

	quadVerts = {
		0.f, finalSize.Get<1>(), -1.f,
		0.f, 0.f, -1.f,
		finalSize.Get<0>(), 0.f, -1.f,
		finalSize.Get<0>(), finalSize.Get<1>(), -1.f
	};

	quadUVs = {
		0.f, 0.f,
		0.f, 1.f,
		1.f, 1.f,
		1.f, 0.f
	};

	glGenVertexArrays(1, &vertexArrayId);
	glBindVertexArray(vertexArrayId);

	glGenBuffers(1, &fbVBO);
	glGenBuffers(1, &fbUV);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, fbVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * quadVerts.size(), &quadVerts[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, fbUV);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * quadUVs.size(), &quadUVs[0], GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);

	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, finalSize.Get<0>(), finalSize.Get<1>(), 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenRenderbuffers(1, &depthStencilId);
	glBindRenderbuffer(GL_RENDERBUFFER, depthStencilId);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, finalSize.Get<0>(), finalSize.Get<1>());

	glGenFramebuffers(1, &frameBufferId);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depthStencilId);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	int i = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (i != GL_FRAMEBUFFER_COMPLETE)
		Logger::Log("Error Creating the frame buffer: %d", i);

	glBindVertexArray(0);
}


FrameBuffer::~FrameBuffer()
{
	glDeleteTextures(1, &textureId);
	glDeleteFramebuffers(1, &frameBufferId);
	glDeleteRenderbuffers(1, &depthStencilId);
}

void FrameBuffer::Activate()
{
	glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void FrameBuffer::Deactivate()
{
	glViewport(0, 0, (GLint)appData->renderWidth, (GLint)appData->renderHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	RenderFBO();
}

void FrameBuffer::SetShader(string s)
{
	shader = ShaderLoader::GetProgramIDAt(s);
	uniformLocations["MVP"] = glGetUniformLocation(shader, "MVP");
	uniformLocations["texture"] = glGetUniformLocation(shader, "texture");

	glm::vec2 screenSize = glm::vec2(appData->renderWidth, appData->renderHeight);
	glm::mat4 matrix = glm::ortho(0.f, screenSize.x, screenSize.y, 0.f, -1.f, 1000.f);

	glUseProgram(shader);
	glUniformMatrix4fv(uniformLocations["MVP"], 1, GL_FALSE, glm::value_ptr(matrix));
	glUseProgram(0);
}

bool FrameBuffer::IsEnabled() const
{
	return isEnabled;
}

bool FrameBuffer::IsGuiIncluded() const
{
	return includeGUI;
}

void FrameBuffer::SetEnabled(bool state)
{
	isEnabled = state;
	if (!state)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
}

void FrameBuffer::SetIncludeGUI(bool state)
{
	includeGUI = state;
}

void FrameBuffer::RenderFBO()
{
	glDisable(GL_DEPTH_TEST);
	glBindVertexArray(vertexArrayId);
	glUseProgram(shader);

	int k = 0;
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glUniform1i(uniformLocations["texture"], k);

	k++;
	for (auto const it : textures)
	{
		glActiveTexture(GL_TEXTURE0 + k);
		glBindTexture(GL_TEXTURE_2D, it.second);
		glUniform1i(uniformLocations[it.first], k);
		k++;
	}

	for (auto const it : intMap)
		glUniform1i(uniformLocations[it.first], it.second);
	intMap.clear();

	for (auto const it : floatMap)
		glUniform1f(uniformLocations[it.first], it.second);
	floatMap.clear();

	for (auto const it : vec2Map)
		glUniform2f(uniformLocations[it.first], it.second.x, it.second.y);
	vec2Map.clear();

	for (auto const it : vec3Map)
		glUniform3f(uniformLocations[it.first], it.second.x, it.second.y, it.second.z);
	vec3Map.clear();

	for (auto const it : vec4Map)
		glUniform4f(uniformLocations[it.first], it.second.x, it.second.y, it.second.z, it.second.w);
	vec4Map.clear();

	glDrawArrays(GL_QUADS, 0, quadVerts.size() / 3);
	glUseProgram(0);
	glBindVertexArray(0);
	glEnable(GL_DEPTH_TEST);
}

void FrameBuffer::CheckForUniformLocation(string key)
{
	auto it = uniformLocations.find(key);
	if (it == uniformLocations.end())
		uniformLocations[key] = glGetUniformLocation(shader, key.c_str());
}

void FrameBuffer::SetInt(string key, int value)
{
	CheckForUniformLocation(key);
	intMap[key] = value;
}

void FrameBuffer::SetFloat(string key, float value)
{
	CheckForUniformLocation(key);
	floatMap[key] = value;
}

void FrameBuffer::SetVec2(string key, Vector<glm::vec2> value)
{
	CheckForUniformLocation(key);
	vec2Map[key] = value.GetVec();
}

void FrameBuffer::SetVec3(string key, Vector<glm::vec3> value)
{
	CheckForUniformLocation(key);
	vec3Map[key] = value.GetVec();
}

void FrameBuffer::SetVec4(string key, Vector<glm::vec4> value)
{
	CheckForUniformLocation(key);
	vec4Map[key] = value.GetVec();
}

void FrameBuffer::SetTexture(string key, string fileName)
{
	std::vector<std::string> keys;
	std::stringstream ss(key);
	std::string keyPart;

	while (getline(ss, keyPart, ','))
	{
		CheckForUniformLocation(key);
		keys.push_back(keyPart);
	}

	std::vector<std::string> fileNames;
	ss = std::stringstream(fileName);
	std::string fileNamePart;

	while (getline(ss, fileNamePart, ','))
	{
		fileNames.push_back(fileNamePart);
	}

	for (unsigned int i = 0; i < keys.size(); i++)
	{
		auto it = RM->textures.find(fileNames[i]);
		if (it != RM->textures.end())
		{
			textures[keys[i]] = it->second.bufferID;
		}
		else
			Logger::Log("Warning!: Texture %s not found.", fileNames[i].c_str());
	}
}